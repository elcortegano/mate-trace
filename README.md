# mate-trace v1.alpha.2 [![build status](https://gitlab.com/elcortegano/mate-trace/badges/master/pipeline.svg)](https://gitlab.com/elcortegano/mate-trace/commits/master)

A software for the simulation and analysis of mate choice strategies using spatial information.  

## Overview

Mate-trace has been built as a tool for the simulation of biological populations and the investigation of mate choice strategies. Individuals in mate-trace can find partners at random according to their phenotypic frequencies, but they can also move in a scenario where rules for spatial information can be defined. For example, females can leave a 'pheromone' path that can attract males. Furthermore, it provides a complete set of functions for preference, competence and inheritance.

The following section give a summary of the main simulation steps. Details of each of these can be found on the [wiki](https://gitlab.com/elcortegano/mate-trace/wikis/home):

**Building map**
By default, mate-trace creates an scenario in the form of a graph network where individuals can move and interact between them, mainly through competence and mating. This map has also the property to save spatial information that can be used to track individuals' path.

Two algorithms are implemented to build the network map:

- Rectangular: Creates a simple, grid-shaped network. It could represent some plain extension or surface.
- Barabási-Albert: Follows Barabási-Albert model (1999) to build a scale-free network, with some nodes more connected than others following a power law distribution. 


**Population initialization**

Once the map is set, a population of *N* individuals is created, with an expected number of females and males given by a binomial distribution (N, p=0.5). Individuals in the initial base population are not inbred or relatives.

Two phenotypic traits are considered: a quantitative trait potentially related to mate choice and preference models, and unrelated (in principle) with biological fitness, that we call *size* for simplicity. A second considered trait is fecundity, which is related to females ability to generate offspring. Traits values will be initially sampled from a gaussian distribution.

Every individual will be placed on a different location in the map.

**Movement rounds**

Individuals will move sequentially on the map (ladies first). The movement strategies are based on the idea of brownian motion. Thus, everytime an individual move, an initial direction and distance will be sampled at random. Movement is conditioned however to location availability. The movement pattern is gender specific.

Females will move a number of locations given by the sampled value of distance. During movement, the same vertex cannot be visited twice, including the starting point. Note that, as every location in the map is defined as a potential scenario for mating, only one female can be placed in a given location at once.

During their movement, females will leave a trackeable path in the edge connecting each pair of locations they visit, containing information about their phenotype and direction. This is what is referred here as "spatial information". In the current implementation, this information will be overwritten as females go through.

Males base movement equals those of females. The exepction comes when a male is placed in a vertex where at least one neighbor edge contains information of a female in an outgoing direction. In these cases, males will tend to follow the path left by females. Maximum number of males per location is two.

**Mating**

After the movement round, every female placed together with at least one male have a chance of mating. This process is simulated following two steps: male-male competence, and courtship.

In the case that the female is accompanied by more than one male, these males will compete so only one will be candidate to mate with the female. Chances of winning the competence process may depend on the phenotypic values of the males.

Once the candidate mating pair is set, a preference function is called in order to simulate the "courtship" process or the mutual preference between male and female. If mating occurss, the female will save the information of the corresponding male in a spermathecal. As new matings occur, more males information can be saved.

**Season ending and inheritance**

Generations (or mating seasons) can end after a given number of movement (and mating) rounds, number of matings or encounters. The program can also finish early in population becomes extinct (*eg*. because *N* is critically low or there are no individual of both genders).

Everytime a season ends, females in the population are evaluated in order to generate offspring according to the mates they had, and their fecundity. Individuals in the offspring will hace values of their phenotypic values according to the inheritance rules.

As generations are considered to be discrete, the new population will replace the old one, and the individuals will be placed on different locations as before, initiating new movement and mating rounds until the desired number of generations has been run.

## Quick install

You can download mate-trace from the terminal by entering:

```
git clone https://gitlab.com/elcortegano/mate-trace.git
```

It can be compiled from source by using `make`. See the Wiki to read more installation details. 

```
cd mate-trace/
make
sudo make install
```

Find more information about alternative ways of installation in the [wiki](https://gitlab.com/elcortegano/mate-trace/wikis/home).

## Options

Several options can be passed in the command line terminal when calling the software. In short, these options can be passed similarly as for GNU/Linux utilities using a *short* form preceded by a single dash ('-') or in a long form with double dash ('--'). Single-dash options are named with only one character and can be grouped together with the same dash character. Examples of use are given below. 

Some options require an argument or value, this can be entered separated from the corresponding option by space (' ') or equal ('=') sign. The type of that value is given with the option name. If absent, no value is required.

Available options are:

### General options

  **Help**: Displays a help message and exits the program.  
  `-h`, `--help`  
  **Name [TEXT]**: Gives a name to the analysis run, that will be used as prefix for all output files (with '_' as delimiter).
  `-n`, `--name`  
  **Seed [INT]**: Sets a seed integer value for pseudo-random number generation. Default is current time.  
  `-s`, `--seed`  
  **Generations [INT]**: The number of generations to simulate. Default is 5.  
  `-t`, `--generations`  
  **Iterate**: Runs *t* replicates of the pase population, instead of simulating *t* generations.  
  `-i`, `--iterate`  
  **Save log**: Saves a log file that records all the options used.  
  `--save-log`  

### Map options
  **Map shape [TYPE]**: Defines the algorithm to build the map network. Valid types are rectangular (`rectangular`,`square`,`r`,`s`) and Barabási-Albert (`barabasi-albert`, `barabasi`, `ba`). Default is rectangular.  
  `-T`, `--shape`  
  **Map size [INT]**: Graph order (total number of vertices, or positions available). Default is 10.  
  `-V`, `--Map-size`  
  **Length [INT]**: The length in *rectangular shaped* maps.  
  `-l`, `--length`  
  **Width [INT]**: The width in *rectangular shaped* maps.  
  `-w`, `--width`  

### Population options  
  **Population size [INT]**: Total population size. Default is 3.  
  `-N`, `--Pop-size`  
  **Migration rate [NUM]**: The migration rate per generation, given as a fraction between 0 an 1.  
  `-m`, `--migrants`  
  **One migration per generation**: Sets a migration rule of one migrant per generation.  
  `--ompg`  
  **Reproductive limiting rate [INT]**: Sets a maximum number of offspring per female. Default is 20.  
  `-R`, `--limit-rate`  
  **Inbreeding depression rate [NUM]**: Sets a value of inbreeding depression rate. Default is 0.0.  
  `-b`, `--delta`  
  **Purging coefficient [NUM]**: Sets a vlaue for the purging coefficient. Default is 0.0.  
  `-d`, `--d`  
  **Aging [INT]**: Sets a minimum age to sterility, where age is measured as the number of successful matings. It also defines a mating cost for males. Default is 0, which indicates no aging.  
  `-a`, `--aging`  
  **Season time [INT]**: Maximum number of movements allowed per mating season. Default time equals map size (`--Map-size`)   
  `--season-time`  
  **Number of matings [INT]**: Required number of successful matings to end season.  
  `--n-matings`  
  **Number of encounters [INT]**: Required number of total encounters per season.  
  `--n-encounters`  

## Biological traits
  **Generic additive and environmental variances [NUM]**: Sets the additive and environmental variance. Default are trait-specific.  
  `--Va`  
  `--Ve`  
  **Body size [NUM]**: Defines the initial mean for body size (the quantitative trait used for preference), as well as their additive and environmental variances. It can be defined for females and males separately. Default is 5.0.  
  `--size`  
  `--size.fem`  
  `--size.mal`  
  `--size.Va`  
  `--size.Ve`  

  **Fecundity [NUM]**. Sets the relationship between the additive value of fecundity, and that of the trait (body size), following the expression A(fecundity) = min(fecundity) + k·A(trait).
  `--fec.min`
  `--fec.k`
  `--fec.Ve`

  **Body size dimorphism [NUM]**: Defines how the females are larger than males if positive, otherwise males will be larger. By default there is no dimorphism.  
  `-D`, `--dimorphism`  
  `--D.Va`  
  `--D.Ve`  

  **Movement type [TYPE]**: The type of movement for individuals. Valid types are pseudo-brownian (`pseudo-brownian`, `pseudo-b`, `pb`, `p`) and one-step movement (`one`, `1`). Default is pseudo-brownian.  
  `-M`, `--movement`  
  **Distribution of the distance moved [NUM NUM]**: Defines the mean and standard deviation parameters of a normal distribution to sample the distance moved by individuals each round. These parameters can be defined globally or separately for females and males.  
  `--dist.mov`  
  `--dist.mov.fem`  
  `--dist.mov.mal`  
  **Path time [INT]**: Sets the maximum number of movement rounds that the path is saved on an edge. Default value is 0, which indicates no limit.  
  `--path-time`  
  **No path**: Females won't leave any traceable path on the map.  
  `--no-path`  

### Mate choice parameters
  **Choice parameter [NUM]**: Sets the initial parameter of mate choice in the range [-1.0, 1.0], as well as their additive and environmental variances. It may be fefined for females and males separately. Default is 0.0.  
  `-C`, `--choice`  
  `--C.fem`  
  `--C.mal`  
  `--C.Va`  
  `--C.Ve`  
  **Choosy individuals**: Sets choosy males or females, meaning that these will be the individuals who choose their mating partners.  
  `--choosy-males`  
  `--choosy-females`  
  **Tolerance [NUM]** : Sets the tolerance parameter (s). Default is 0.1.  
  `-S`, `--tolerance`  
  **Bias [NUM]** : Sets the bias parameter (default is 0.0).  
  `-B`, `--bias`  
  **Male-male competence [TYPE]**: Sets the competence mode for males. Valid types are `size` and `random`. Default is size.  
  `--mm-comp`  
  **Preference model [TYPE]**: Defines the preference model. The current implementation includes gaussian FND (`gaussian-fnd`, `fnd`) and BD03 (`gaussian-bd03`, `bd03`) models. It also includes `random` and `first-sight` models. Default is gaussian FND.  
  `-P`, `--preference`    
  **Monogamy**: Enables monogamic behavior for individuals in the population.  
  `--monogamy`  

### Random encounters
  **Random encounters**: Sets random encounters (*i.e.* remove spatial factor, and individuals will meet according to their frequencies). These encounters will occur by sampling with replacement. To set random encounters without replacement, enable `monogamy`.  
  `-r`, `--random`  
  **Try hard**: Under random mating, forces every female (or male) to try mating. Each female (or male) will encounter every male (or female) in the population until one mating happens or all pairs are rejected.  
  `--try-hard`  

### Output
  **Save meetings**: Saves all individual meetings in the registry output files, not only succesful ones (mates).  
  `--save-meetings`  

## Examples

Here are shown some use examples. As a simplest way of usage, the program can be run just with four parameters

```
mate-trace -N100 -t100 -V1000 --n-matings=100
mate-trace -N100 -t100 -r --n-matings=100
```

In the first example, a base population of size 100 is initiated and simulated for 100 generations in a map of size 1000, with 100 matings being evaluated per generation. In the second example, matings occurr at random, without simulating movement in a graph.

It is usually convenient however to change the default behavior in order to explore different mating conditions. For reproducibility, we will use below a seed value. The number of individuals and map size will also be modified.

```
./mate-trace -N100 -t100 -V 1000 --seed=1234 --season-time 100
```
Above are shown four different ways to call options. One-letter options are invoqued with single dash, and can be followed by the parameter value by a space. Double-dash is used for long options, and use space or equal sign to precede parameter values. Some options however don't need a value, as it occurs for example when monogamy is enabled:

```
./mate-trace -N100 -V 1000 -t 100 --monogamy -s 1234 --season-time 100
```
In this last call we have also changed the way to set the seed, which is an option that can be set both using the sort or the long form.

Finally, some options require special parameter values to modify the default values. Below, we will perform a simulation in a map built using the scale free algorithm, and configure a one-step movement strategy:

```
./mate-trace -N 100 -t100 -V 1000 -s 1234 --season-time 100 -T ba --movement=one -m 0.01
```
Note that these parameters are entered simply as text, without needing quotation marks. Similarly, a migration rate of 0.01 has been set, using point as decimal delimiter.

To enter custom distributions for some biological traits, two values need to be given to the corresponding option. For example, we could set a movement distance for females given by a distribution N(5,0) and N(4,1) for males as:

```
./mate-trace -N100 -t100 -V 1000 -s 1234 --dist.mov.fem 5 0 --dist.mov.mal=4 1 -C 0.1 --n-matings 100
```

Note than in the example above, the body size of females is fixed (sd=0). Initial choice has mean 0.1 for both genders.

## Assistance

Please read the full `README` file before running the program and/or asking any question about it. Further details can be found on the project's [Wiki](https://gitlab.com/elcortegano/mate-trace/wikis/home).

If you have found a bug, want to request an additional feature, or don't understand an option, please open a new issue (with the right tag and including as much information as possible) in the gitlab [issue tracker](https://gitlab.com/elcortegano/mate-trace/issues) (it will require an account).

## References

Some bibliographic references highlighted above are:

- Barabási AL, Albert R (1999) *Emergence of scaling in random networks*. Science 15 (286): 509-512.

## License

mate-trace: Software for the simulation and analysis of mate choice strategies using spatial information.

Copyright (C) 2018 Eugenio López-Cortegano  (Universidad de Vigo).

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
