FROM alpine:3.4

LABEL version="v1.alpha.0"
LABEL description="A software for the analysis and simulation of matting choice of individuals under different conditions of intra and inter-gender competence."
LABEL maintainer="e.lopez@uvigo.es"

RUN apk update && apk add \
    gcc \
    g++ \
    make \
    git \
    && git clone https://gitlab.com/elcortegano/mate-trace.git \
    && cd mate-trace \
    && make \
    && cp mate-trace /bin \
    && rm -r /mate-trace \
    && apk del g++ make git

WORKDIR /tmp

ENTRYPOINT ["mate-trace"]
