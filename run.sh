#!/bin/sh

#SBATCH -N 1
#SBATCH --qos=shared
#SBATCH -p shared
#SBATCH -t 1:00:00
#SBATCH --cpus-per-task=1

N=$2
REP=10000
FEM_MEAN=$1
EVAL=$2
BIAS=$3
C=$4

OUTDIR=${STORE}/fem${FEM_MEAN}_eval${EVAL}_bias${BIAS}_C${C}/
mkdir -p $OUTDIR
cp mate-trace $OUTDIR
cd $OUTDIR
./mate-trace -i -r --choosy-males --try-hard -t ${REP} -s 1234 -N ${N} -C ${C} --C.Va 0.0 --C.Ve 0.0 --size.mal 0.0 --size.fem ${FEM_MEAN} --size.Ve 0.0 --bias ${BIAS} --save-log
rm mate-trace edg* vert* mor* gra*
mv pedigree.csv ${OUTDIR}pedigree_fem${FEM_MEAN}_eval${EVAL}_bias${BIAS}_C${C}.csv
mv registry.csv ${OUTDIR}registry_fem${FEM_MEAN}_eval${EVAL}_bias${BIAS}_C${C}.csv
