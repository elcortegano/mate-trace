#!/bin/sh

module load gcc/6.4.0
module load glib
make clean && make
declare -a FEM_MEAN=("0.0" "0.5" "1.0" "-0.5" "-1.0")
declare -a EVAL=("100")
declare -a BIAS=("0.0" "0.25" "0.5")
declare -a C=("0.0" "0.1" "0.2" "0.3" "0.4" "0.5" "0.6" "0.7" "0.8" "0.9" "1.0")

for fem in "${FEM_MEAN[@]}"; do
    for eval in "${EVAL[@]}"; do
        for bias in "${BIAS[@]}"; do
            for c in "${C[@]}"; do
		sbatch ./run.sh $fem $eval $bias $c
            done
        done
    done
done
