/* =======================================================================================
 *                                                                                       *
 *       Filename: population.cpp                                                        *
 *                                                                                       *
 *    Description: A population is a mixture of individuals. Some properties and methods *
 *                 are defined at this level.                                            *
 *                                                                                       *
 ======================================================================================= */

#include "population.h"
#include "genetics.h"

/* ***************************************************************************************
 * Constructor & destructor
 * ***************************************************************************************/
Population::Population ( const Population_parameters& pop_info, Graph& map, const Genetic_parameters& genetics, const Matechoice_parameters& mc_info, const Output_parameters& out) {

	// Determine number of males, females and migrants
	generation = 0;
	N = pop_info.N_inds;
	max_N = N;
	N_ladies = 0;
	pop_genetics = genetics;
	n_migrants = 0;
	if (pop_info.migration > 0.0) {
		n_migrants = std::round(pop_info.migration*N);
		if (!n_migrants) n_migrants = 1;
	}
	survival_dist.set(genetics.size.mean_mal, genetics.size.Va);
	output = out;

	// Generate the population
	if (N > map.get_order()) throw_error (ERROR_POPUL_SIZEMAP);
	random_population (population);
	population[0]->set_move_type(pop_info.movement); // sets movement type
	for (std::size_t i(0); i<population.size(); ++i) {
		if (!population[i]->ask_gender()) ++N_ladies;
		else break;
	}

	// Computes the coancestry matrix
	coancestry = initialize_coancestry (N);
	if (genetics.d>0.0) purged_coancestry = coancestry;

	// Place individuals in the map
	this->spread_individuals(map);

	// Set other population parameters
	parameters = pop_info;
	n_old_members = 0;
	nmatings = 0;

	// Initialize mate choice parameters
	pop_mchoice.init(*this, mc_info);
	choosy = mc_info.choosy;

}

Population::~Population () {
	kill (population);
}

/* ***************************************************************************************
 * Individuals activity
 * ***************************************************************************************/

/* spread_individuals: This functions distributes individuals among a given graph. In other
 * words, it initializes the position of individuals.
 * --------------------------------------------------------------------------------------*/
void Population::spread_individuals ( Graph& map) {

	// Initialize vector of locations
	map.clear();
	ent order (map.get_order());
	iVector possible_locations;
	from_zero_to(possible_locations, order);

	// Randomize
	shuffle(std::begin(possible_locations), std::end(possible_locations), rnd_generator);

	// Update population census
	for (ent i(0); i<N; ++i) {
		population[i]->place(map, possible_locations[i]);
	}

	//map.print_graph(*this);
}

/* move_individuals: This functions let individuals move through a given graph.
 * --------------------------------------------------------------------------------------*/
void Population::move_individuals ( Graph& map ) {

	for (ent i(0); i<N; ++i) {
		Individual* Ind (population[i]);

		// Check individual availability
		if (Ind->had_enough()) continue;

		// Males priorize mating or feeding
		if (Ind->is_male() && parameters.male_cost) Ind->focus();

		// Sampling distance
		ent distance_i (Ind->go_distance());

		// Sampling direction
		iVector init_paths (Ind->search_direction(map));
		iVector itinerary (1, Ind->ask_location());

		// The individual traces a candidate path (itinerary)
		while (distance_i) {
			ent destination_i (Ind->sample_destination(init_paths, map));
			if (destination_i != itinerary[itinerary.size()-1]) {
				itinerary.push_back(destination_i);
				init_paths = Ind->search_direction_from(map, destination_i);
				diff (init_paths, itinerary);
				--distance_i;
				// Males path always end when they find a female
				if (Ind->is_male() && map.get_vertex(destination_i).get_Nfem()) distance_i = 0;
			} else distance_i = 0;
		}

		// The individual move to an available location in the itinerary
		Ind->move(itinerary, map);

	}
	//map.print_graph(*this);
}

/* mate_individuals: This function searches females that share a vertex with a male, and
 * determines if they mate.
 * --------------------------------------------------------------------------------------*/
void Population::mate_individuals (Graph& map) {

	// CHeck suitability for monogamy
	if (parameters.monogamy && parameters.max_nmatings) {
		if (parameters.max_nmatings > N_ladies || (parameters.max_nmatings > (population.size()-N_ladies))) {
			throw_error(ERROR_INDIV_NOMONOG);
		}
	}

	for (ent i(0); i<N_ladies; ++i) {
		Individual* female (population[i]);

		// Check female availability
		if (female->had_enough()) continue;

		// Check female location and company
		Vertex location (map.get_vertex(female->ask_location()));
		if (!female->is_alone(map) && location.get_Nmal()) {

			// Choose a male partner (male-male competence)
			std::list<Individual*>::iterator male_it = pop_mchoice.mm_competence(location.get_mal());
			Individual* male = *male_it;

			// Call preference function and mate
			if (pop_mchoice.match(female, male)) {
				if (!parameters.monogamy) female->mate(male,map);
				else female->marry(male, map);
				log_matings (female, male, true);
			} else log_matings (female, male, false);
		}

		if (parameters.max_nmatings && nmatings >= parameters.max_nmatings) break;
		else if (parameters.max_nmeetings && registry.size() >= parameters.max_nmeetings) break;
	}
}

/* random_mating: This functions searches (at random) a partner for every female, and calls
 * the corresponding preference function. Male-male competence is ignored.
 * --------------------------------------------------------------------------------------*/
void Population::random_mating (Graph& map, bool replacement) {

	// CHeck suitability for monogamy
	if (parameters.monogamy && parameters.max_nmatings) {
		if (parameters.max_nmatings > N_ladies || (parameters.max_nmatings > (population.size()-N_ladies))) {
			throw_error(ERROR_INDIV_NOMONOG);
		}
	}

	std::list<ent> male_pool;
	std::vector<ent> forbidden_fem;
	for (ent i(N_ladies); i<N; ++i) male_pool.push_back(i);

	// When sampled without reference, there must be at least as many males as females
	if (male_pool.size() < N_ladies && !replacement) {
		ent diff (N_ladies-male_pool.size());
		while (forbidden_fem.size() < diff) {
			ent id (sample_from_zero_to(N_ladies-1));
			if (!is_in(forbidden_fem, id)) forbidden_fem.push_back(id);
		}
	}

	for (ent i(0); i<N_ladies; ++i) {

		if (!replacement && is_in(forbidden_fem, i)) continue;

		// Choose random index for male and female
		ent fem_index (i);
		if (replacement) fem_index = sample_from_zero_to (N_ladies-1);
		ent mal_index (sample(male_pool));
		Individual* female (population[fem_index]);
		Individual* male (population[mal_index]);
		if (female->had_enough() || male->had_enough()) continue;

		// Call preference function and mate
		if (pop_mchoice.match(female, male)) {
			if (!parameters.monogamy) female->mate(male,map);
			else female->marry(male, map);
			if (!replacement) male_pool.remove(mal_index);
			log_matings (female, male, true);
		} else log_matings (female, male, false);

		if (parameters.max_nmatings && nmatings >= parameters.max_nmatings) break;
		else if (parameters.max_nmeetings && registry.size() >= parameters.max_nmeetings) break;
	}

}

/* tryhard_mating: This functions loops over all females (or males) and forces them to
 * encounter to every male (or female) until there is a mate, or until all pairs are rejected.
 * --------------------------------------------------------------------------------------*/
void Population::tryhard_mating (Graph& map) {

	if (pop_mchoice.get_mc().choosy == FEMALES) {
		for (ent i(0); i<N_ladies; ++i) {
			Individual* female (population[i]);
			iVector male_list;
			from_to(male_list, N_ladies, N-1);
			shuffle(std::begin(male_list), std::end(male_list), rnd_generator);
			bool mate (false);
			while (!mate && male_list.size()) {
				ent fem_index (male_list[male_list.size()-1]);
				Individual* male (population[fem_index]);
				if (pop_mchoice.match(female, male)) {
					if (!parameters.monogamy) female->mate(male,map);
					else female->marry(male, map);
					log_matings (female, male, true);
					mate = true;
				} else {
					log_matings (female, male, false);
					male_list.pop_back();
				}
			}
		}
	} else if (pop_mchoice.get_mc().choosy == MALES) {
		for (ent i(N_ladies); i<N; ++i) {
			Individual* male (population[i]);
			iVector female_list;
			from_zero_to(female_list, N_ladies-1);
			shuffle(std::begin(female_list), std::end(female_list), rnd_generator);
			bool mate (false);
			while (!mate && female_list.size()) {
				ent fem_index (female_list[female_list.size()-1]);
				Individual* female (population[fem_index]);
				if (pop_mchoice.match(female, male)) {
					if (!parameters.monogamy) female->mate(male,map);
					else female->marry(male, map);
					log_matings (female, male, true);
					mate = true;
				} else {
					log_matings (female, male, false);
					female_list.pop_back();
				}
			}
		}
	} else throw_error(ERROR_MCHOI_CHOHARD);

}

/* contributions: Calculates a vector with the contributions of each female, according to
 * their fecundities. If total contributions are above the limiting population size, a
 * bottleneck is produced.
 * --------------------------------------------------------------------------------------*/
iVector Population::contributions () {

	iVector contributions;
	ent total_contrib (0);

	// Contributions are calculated for each female depending on their fecundity
	for (ent i(0); i<N_ladies; ++i) {
		if (population[i]->is_pregnant()) contributions.push_back(population[i]->ask_fecundity());
		else contributions.push_back(0);
		if (contributions[i] > parameters.R) contributions[i] = parameters.R;
		total_contrib += contributions[i];
	}

	return contributions;
}

/* birth: This function checks all females sequenctially, and generate candidate offspring
 * according to their fecundity.
 *
 * Note individuals are named as '0' (invalid). They must be renamed after survival.
 * --------------------------------------------------------------------------------------*/
void Population::birth (std::vector<Female>& d, std::vector<Male>& s, const iVector& contrib) {
	Bernouilli sample_gender;

	// Generate vector of female and male offspring separately
	iVector daughters;
	daughters.resize(N_ladies, 0);
	iVector sons (daughters);
	for (ent i(0); i<N_ladies; ++i) {
		if (contrib[i]) {
			for (ent j(0); j<contrib[i]; ++j) {
				if (sample_gender(rnd_generator)) ++sons[i];
				else ++daughters[i];
			}
		}
	}

	// If there is migration, sample the number of females and males
	ent n_fem_migrants (0);
	if (n_migrants) n_fem_migrants = sample_binomial(n_migrants,0.5);

	// Generate daughters first
	for (ent i(0); i<N_ladies; ++i) {
		if (contrib[i]) {
			Individual* mother (population[i]);
			for (ent j(0); j<daughters[i]; ++j) {
				Female daughter (0, mother, coancestry, purged_coancestry, pop_genetics, n_old_members);
				if (survival_dist.sim_survival(daughter)) {
					d.push_back(daughter);
				}
			}
		}
	} // female migrants
	for (ent i(0); i<n_fem_migrants; ++i) {
		Female migrant (0, pop_genetics);
		if (survival_dist.sim_survival(migrant)) {
			d.push_back(migrant);
		}
	}

	// Then males
	for (ent i(0); i<N_ladies; ++i) {
		if (contrib[i]) {
			Individual* mother (population[i]);
			for (ent j(0); j<sons[i]; ++j) {
				Male son (0, mother, coancestry, purged_coancestry, pop_genetics, n_old_members);
				if (survival_dist.sim_survival(son)) {
					s.push_back(son);
				}
			}
		}
	}
	for (ent i(0); i<(n_migrants-n_fem_migrants); ++i) {
		Male migrant (0, pop_genetics);
		if (survival_dist.sim_survival(migrant)) {
			s.push_back(migrant);
		}
	}

}

/* random_population: Generates a random population of females and males.
 * --------------------------------------------------------------------------------------*/
void Population::random_population ( std::vector<Individual*>& pop) {

	// Get a number of females
	ent N_fem = sample_binomial(N,0.5);
	while (!N_fem || N_fem ==N) {
		N_fem = sample_binomial(N,0.5);
	}

	// Create population vector. Ladies first
	for (ent i(0); i<N_fem; ++i) {
		pop.push_back(new Female (++ids, pop_genetics));
	}
	for (ent i(N_fem); i<N; ++i) {
		pop.push_back(new Male (++ids, pop_genetics));
	}
}

/* retirement: Aged individuals may become sterile, and are removed from the population
 * --------------------------------------------------------------------------------------*/
void Population::retirement (Graph& map) {
	if (!parameters.aging) return;
	for (ent i(0); i<N; ++i) {
		Individual* Ind (population[i]);
		ent age (Ind->ask_age());
		bool sterile (false);
		if (age>=parameters.aging) {
			coefficient alpha = 5.0;
			coefficient beta = 2.0;
			ent t (age);
			ent gamma (parameters.aging);
			coefficient Rt = exp(-pow(((t-gamma)/alpha),beta));
			coefficient U = sample_prob();
			if (U>=Rt) sterile = true;
		}
		if (sterile) Ind->exit(map);
	}
}

/* ***************************************************************************************
 * Environmental methods
 * ***************************************************************************************/

/* extinction: Checks if population is viable or not.
 * --------------------------------------------------------------------------------------*/
bool Population::extinction () {
	if (N<2 || N_ladies==N || !N_ladies) return true;
	else return false;
}

/* mutation: Genetic variance increases due to new mutation.
 * --------------------------------------------------------------------------------------*/
void Population::mutation () {
	mutation(pop_genetics.size);
	mutation(pop_genetics.choice);
}

void Population::mutation (Trait_parameters& trait) {
	trait.Va += V_M (trait.Ve);
}

/* season_ends: Season ends when all females have mate (or tried it?) at least X times with
 * males. Individuals will then have offspring, and a new population will be generated.
 * --------------------------------------------------------------------------------------*/
bool Population::season_ends (Graph& map, ent& remaining_generations, const std::string& name, bool iterate) {

	static ent count (0);
	++count;

	// Individuals age
	retirement (map);

	// If at least one female does not match requirements, season continues
	bool season_end (false);
	if (parameters.max_nmatings && nmatings >= parameters.max_nmatings) season_end = true;
	else if (parameters.max_nmeetings && registry.size() >= parameters.max_nmeetings) season_end = true;
	else if (count >= parameters.season_time && !parameters.max_nmatings && !parameters.max_nmeetings) season_end = true;
	if (!season_end) {
		pop_mchoice.update(*this);
		return false;
	}
	count = 0;

	// If season ends, a new generation of individuals borns
	this->save_pedigree (name);
	this->save_registry (name, output.save_meetings);
	std::vector<Individual*> new_population;
	if (!iterate) {
		std::vector<Female> daughters;
		std::vector<Male> sons;
		iVector contrib (this->contributions());
		birth (daughters, sons, contrib); // includes selection

		// Population size is limited
		ent noffspring (daughters.size()+sons.size());
		if (noffspring > max_N) {
			ent excess (noffspring - max_N);
			iVector remove;
			while (excess) {
				ent index (sample_from_zero_to(noffspring-1));
				if (!is_in(remove, index)) {
					remove.push_back(index);
					--excess;
				}
			}
			for (ent i(0); i<noffspring; ++i) {
				if (!is_in(remove,i)) {
					ent index (i);
					if (i >= daughters.size()) {
						index -= daughters.size();
						new_population.push_back(new Male (sons[index]));
					} else new_population.push_back(new Female (daughters[index]));
				}
			}
		}
	} else random_population (new_population);

	// Rename individuals
	for (ent i(0); i<new_population.size(); ++i) {
		new_population[i]->rename(++ids);
	}

	// Determine number of females
	ent N_daughters (0);
	for (std::size_t i(0); i<new_population.size(); ++i) {
		if (!new_population[i]->ask_gender()) ++N_daughters;
		else break;
	}

	// Set new population
	set_new_population (new_population, N_daughters);
	if (!iterate) mutation();
	--remaining_generations;

	// Check population viability
	if (remaining_generations && this->extinction()) {
		throw_warning(WARNG_POPUL_EXTINCT);
		remaining_generations = 0;
		return true;
	}

	// Place individuals in the map
	this->spread_individuals(map);

	// Recalculate mate choice parameters
	if (remaining_generations) pop_mchoice.init(*this, pop_mchoice.get_mc());

	return true;
}

/* ***************************************************************************************
 * Set methods
 * ***************************************************************************************/

/* update_inbreeding: Updates the coancestry matrix and pedigree of the population.
 * --------------------------------------------------------------------------------------*/
void Population::update_inbreeding () {

	// Update standard inbeeding
	for (ent i(0); i<population.size(); ++i) {
		Deck f;
		for (ent j(0); j<pedigree.size(); ++j) {
			f.push_back(f_AB(population[i], pedigree[j], coancestry, n_old_members));
		}
		for (ent j(0); j<i+1; ++j) {
			Pedigree_record ind_B (ind_to_ped(population[j], 0.0, generation));
			f.push_back(f_AB(population[i], ind_B, coancestry, n_old_members));
		}
		coancestry.push_back(f);
	}

	// Update purged inbreeding
	if (pop_genetics.d > 0.0) {
		for (ent i(0); i<population.size(); ++i) {
			Deck g;
			for (ent j(0); j<pedigree.size(); ++j) {
				g.push_back(g_AB(population[i], pedigree[j], purged_coancestry, pop_genetics.d, n_old_members));
			}
			for (ent j(0); j<i+1; ++j) {
				Pedigree_record ind_B (ind_to_ped(population[j], pop_genetics.d, generation));
				g.push_back(g_AB(population[i], ind_B, purged_coancestry, pop_genetics.d, n_old_members));
			}
			purged_coancestry.push_back(g);
		}
	}

}

/* set_new_population: Gets a vector of new individuals and set a new population.
 * --------------------------------------------------------------------------------------*/
void Population::set_new_population (const std::vector<Individual*>& new_pop, ent N_daughters) {
	log_individuals();
	++generation;
	kill (population);
	population = { std::make_move_iterator(std::begin(new_pop)), std::make_move_iterator (std::end(new_pop)) };
	N = population.size();
	N_ladies = N_daughters;
	update_inbreeding();
	clear_pedigree ();
}

/* log_individuals: Appends basic individuals information into a pedigree record.
 * --------------------------------------------------------------------------------------*/
void Population::log_individuals () {
	Pedigree new_pedigree;
	for (auto& ind: population) {
		Pedigree_record ped (ind_to_ped(ind, pop_genetics.d, generation));
		new_pedigree.push_back(ped);
	}
	pedigree = new_pedigree;
}

/* log_matings: Records matings into a civil registry.
 * --------------------------------------------------------------------------------------*/
void Population::log_matings (Individual* female, Individual* male, bool mated) {
	Registry reg;
	reg.t = generation;
	reg.ID_female = female->ask_name();
	reg.ID_male = male->ask_name();
	reg.P_female = female->ask_size();
	reg.P_male = male->ask_size();
	reg.success = mated;
	reg.C = male->ask_choice();
	if ((reg.C < female->ask_choice() && choosy==BOTH) || choosy==FEMALES) reg.C = female->ask_choice();
	registry.push_back(reg);
	if (mated) ++nmatings;
}

/* clear_pedigree: Remove all pedigree records
 * --------------------------------------------------------------------------------------*/
void Population::clear_pedigree () {
	for (ent i(0); i<pedigree.size(); ++i) {
		if (pedigree[i].t == generation-1) {
			coancestry.pop_front();
			for (auto& j: coancestry) j.pop_front();
			if (pop_genetics.d>0.0) {
				purged_coancestry.pop_front();
				for (auto& j: purged_coancestry) j.pop_front();
			}
			++n_old_members;
		}
	}
}

/* ***************************************************************************************
 * Save methods
 * ***************************************************************************************/

/* survey: Prints basic information about individuals in the population (for checking
 * purposes).
 * --------------------------------------------------------------------------------------*/
void Population::survey () {
	static ent count (0);
	std::cout << "Population survey call " << ++count << std::endl;
	for (ent i(0); i<N; ++i) {
		std::cout << "ind (" << population[i]->ask_name()
				  << "), gender " << population[i]->ask_gender()
				  << ", located in " << population[i]->ask_location()
				  << std::endl;
	}
}

/* save_pedigree: Saves an output pedigree file.
 * --------------------------------------------------------------------------------------*/
void Population::save_pedigree (const word& prefix) {

	static ent count (0);

	// Save header
	std::string filename ("pedigree.csv");
	if (prefix != EMPTY) filename = prefix + DELIM + filename;
	std::ofstream outfile;
	if (!count) {
		outfile.open(filename);
		outfile << "ind,mom,dad,gender,size,dimorphism,fecundity,mates,F,C,B";
		if (pop_genetics.d>0.0) outfile << ",g";
		outfile << ",t";
		outfile << std::endl;
	} else outfile.open(filename, std::ios::app);

	// Save pedigree
	for (auto& ind: population) {
		outfile << ind->ask_name() << ','
				<< ind->ask_name_mom() << ','
				<< ind->ask_name_dad() << ','
				<< ind->ask_gender() << ','
				<< ind->ask_size() << ','
				<< ind->ask_dimorphism() << ','
				<< ind->ask_fecundity() << ','
				<< ind->ask_Nmates() << ','
				<< ind->ask_F() << ','
				<< ind->ask_choice() << ','
				<< ind->ask_bias();
				if (pop_genetics.d> 0.0) outfile << ',' << ind->ask_g();
		outfile << ',' << generation;
		outfile << std::endl;
	}
	outfile.close();

	// Clear pedigree
	++count;
}

/* save_registry: Saves an output pedigree file.
 * --------------------------------------------------------------------------------------*/
void Population::save_registry (const word& prefix, bool save_meetings) {

	static ent count (0);

	// Save header
	word filename ("registry.csv");
	if (prefix != EMPTY) filename = prefix + DELIM + filename;
	std::ofstream outfile;
	if (!count) {
		outfile.open(filename);
		outfile << "female,fsize,male,msize,mated,C,t" << std::endl;
	} else outfile.open(filename, std::ios::app);

	// Save registry
	for (ent i(0); i<registry.size(); ++i) {
		if (registry[i].success || save_meetings) {
			outfile << registry[i].ID_female << ','
					<< registry[i].P_female << ','
					<< registry[i].ID_male << ','
					<< registry[i].P_male << ','
					<< registry[i].success << ','
					<< registry[i].C << ','
					<< registry[i].t;
			outfile << std::endl;
		}
	}
	outfile.close();

	// Clear registry
	registry.clear();
	nmatings = 0;
	++count;
}
