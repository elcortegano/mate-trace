/* =======================================================================================
 *                                                                                       *
 *       Filename: graph.cpp                                                             *
 *                                                                                       *
 *    Description: Defines the network that will be used as geographic map.              *
 *                                                                                       *
 ======================================================================================= */

#include "graph.h"

/* ***************************************************************************************
 * Constructor & destructor
 * ***************************************************************************************/
Graph::Graph ( const Graph_parameters& parameters) {

	order = parameters.N_vertices;
	ent length = parameters.length;
	ent width = parameters.width;
	size = 0;

	// Determine graph "shape"
	appearance = parameters.shape;

	// Build network
	if (appearance == RECTANGULAR) {
		if (!length && !width) build_square ();
		else {
			if (!length) length = order/width;
			else if (!width) width = order/length;
			if (width > length) { std::cout << std::endl; throw_error (ERROR_GRAPH_BIGWIDT);}
			order = width*length;
			build_square (width, length);
		}
	} else if (appearance == BA) {
		build_BA(parameters.m0, parameters.m);
	}

	// Set edge properties
	size = edges.size();
	edges[0].set_durability(parameters.path_time);
	edges[0].set_traceability(parameters.track_path);

}

Graph::~Graph () {

}

/* ***************************************************************************************
 * Building
 * ***************************************************************************************/

/* connect: Connect two vertices of the graph with an edge. The ID of the edge its given by
 * the current number of defined edges.
 * --------------------------------------------------------------------------------------*/
void Graph::connect ( ent node_i, ent node_j) {
	ent edge_ij (edges.size());
	vertices[node_i].connect (node_j, edge_ij);
	vertices[node_j].connect (node_i, edge_ij);
	std::pair<ent,ent> key = std::make_pair (node_i, node_j);
	std::pair<ent,ent> key2 = std::make_pair (node_j, node_i);
	nodes_to_edgesmask_map[key] = edge_ij+1;
	nodes_to_edgesmask_map[key2] = edge_ij+1;
	edges.push_back(Edge(edge_ij, node_i, node_j));
}

/* build_square: Methods to build networks with rectangular 'shapes'
 * --------------------------------------------------------------------------------------*/
void Graph::build_square ( ent w_, ent l_) {

	if (!order) throw_error	(ERROR_GRAPH_ZRORDER);
	for (ent ID(0); ID<order; ++ID) {
		vertices.push_back(ID);
	}

	// Local variables
	ent small_side (w_);
	ent long_side (l_);
	ent remain (0);
	bool low_right (true);
	bool go_low (true);

	// Recalculate long side
	if (small_side*small_side != order) {
		ent extra (order-small_side*small_side);
		long_side = small_side + (extra/small_side);
		if (extra %small_side != 0) {
			++long_side;
			remain = extra %small_side;
		}
	}

	// Template of squared network
	ent i_ID (0); // the vertex ID is its coordinate in the 'nodes' vector
	bool bottom (false);
	bool right (false);
	bool left (false);
	for (ent i(0); i<small_side; ++i) {
		if (i==(small_side-1)) bottom = true;
		for (ent j(0); j<long_side; ++j) {
			if ((j+1) %long_side == 0) right = true;
			else right = false;
			if (j% long_side == 0) left = true;
			else left = false;
			if (remain && ((j+2) %long_side == 0)) low_right = false;
			if (remain && right) {
				--remain;
				if (!remain) {
					--long_side;
					go_low = false;
				}
			}

			if (!right) {
				ent j_ID (i_ID+1);
				connect(i_ID, j_ID);
			}
			if (!bottom) {
				ent j_ID (i_ID+long_side);
				connect(i_ID, j_ID);
			}
			if (!right & !bottom & low_right) {
				ent j_ID (i_ID+long_side+1);
				connect(i_ID, j_ID);
			}
			if (!left & !bottom & go_low) {
				ent j_ID (i_ID-1+long_side);
				connect(i_ID, j_ID);
			}

			Coordinate coord = {.x=i+1, .y=j+1};
			vertices[i_ID].set_coord (coord);
			coordinates_to_vertexmask_map[coord] = i_ID+1;
			++i_ID;
			if (!low_right) low_right = true;
			if (!go_low) go_low = true;
			if (i_ID > order) throw_error (ERROR_GRAPH_FITSIZE);

		}
	}
}

void Graph::build_square ( ) {
	// This version tryes to build a square network, but will accept any number of nodes
	build_square (sqrt(order), sqrt(order));
}

/* build_BA: Build a network following Barabási-Albert model
 * --------------------------------------------------------------------------------------*/
void Graph::BA_step (ent ID, ent m, Vector& freqs) {
	iVector connect_history;
	vertices.push_back(ID);

	// Connect node ID to a random node i with pi ~ ki / sum kj
	Vector freq_copy (freqs);
	Vector index (set_index_vector(freqs.size()));
	while (connect_history.size() < m) {
		coefficient current_size (freq_copy[freq_copy.size()-1]);
		coefficient U (sample_prob());
		for (ent i(0); i<index.size(); ++i) {
			ent value (freq_copy[i]);
			ent objective (index[i]);
			coefficient rfreq (value/current_size);
			if (U<=rfreq && !is_in(connect_history, objective)) {
				connect(ID, objective);
				connect_history.push_back(objective);
				if (i) value -= freq_copy[i-1];
				freq_copy.erase(freq_copy.begin()+i);
				index.erase(index.begin()+i);
				for (ent j(i); j<freq_copy.size(); ++j) {
					freq_copy[j] -= value;
				}
				break;
			}
		}
	}

	// Update vector of absolute edge frequencies
	for (ent i(0); i<m; ++i) {
		ent index (connect_history[i]);
		for (ent j(index); j<freqs.size(); ++j) {
			++freqs[j];
		}
	}
	freqs.push_back(m+freqs[freqs.size()-1]);
}

void Graph::build_BA (ent m0, ent m) {

	// Initialization
	if (m0 < 2) throw_error (ERROR_GRAPH_BA_MINM);
	if (order <= 2*m0) throw_error (ERROR_GRAPH_BA_LOWN);
	if (m>m0) throw_error (ERROR_GRAPH_BA_BADM);
	Vector cum_nedges;
	for (ent ID(0); ID<m0; ++ID) {
		vertices.push_back(ID);
	}

	// Initial BA step
	vertices.push_back(m0);
	for (ent i(0); i<m; ++i) {
		connect(m0, i);
		cum_nedges.push_back(i+1);
	}
	cum_nedges.push_back(m+m);

	// BA model
	for (ent ID(m0+1); ID<order; ++ID) {
		BA_step(ID, m, cum_nedges);
	}
}

/* ***************************************************************************************
 * Get methods
 * ***************************************************************************************/
std::vector<Vertex> Graph::get_neighbour_vertices (ent nodeID) const {
	iVector pointed_vertices (this->check_neighbour_nodes(nodeID));
	std::vector<Vertex> ng_vertices;
	for (const auto& index: pointed_vertices) ng_vertices.push_back(vertices[index]);
	return ng_vertices;
}

std::vector<Edge> Graph::get_neighbour_edges (ent nodeID) const {
	iVector pointed_edges (this->check_neighbour_edges(nodeID));
	std::vector<Edge> ng_edges;
	for (const auto& index: pointed_edges) ng_edges.push_back(edges[index]);
	return ng_edges;
}

/* ***************************************************************************************
 * Methods to calculate graph parameters
 * ***************************************************************************************/

/* degree_sequence: Returns the degree sequence of the graph.
 * --------------------------------------------------------------------------------------*/
void Graph::degree_sequence () {
	for (const auto& vertex: vertices) degree_seq.push_back(vertex.degree());
	std::sort (degree_seq.begin(), degree_seq.end(), higher_than);
}

/* P: Returns the frequency of nodes of degree k in the graph.
 * --------------------------------------------------------------------------------------*/
coefficient Graph::P (ent k) const {
	coefficient Pk (0.0);
	for (const auto& i: degree_seq) {
		if (i>k) continue;
		else if (i == k) Pk += 1.0;
		else break;
	}
	return Pk/order;
}

/* ***************************************************************************************
 * Individual-interactivity methods
 * ***************************************************************************************/

/* move_ind: Individuals move between vertices in the map.
 * --------------------------------------------------------------------------------------*/
void Graph::move_ind ( Individual* ind, ent to ) {
	if (ind->ask_gender()) {
		vertices[to].incoming_male(ind);
	} else {
		vertices[to].incoming_female(ind);
	}
}

void Graph::move_ind ( Individual* ind, ent from, ent to ) {
	if (ind->ask_gender()) {
		vertices[from].outgoing_male(ind);
		vertices[to].incoming_male(ind);
	} else {
		vertices[from].outgoing_female(ind);
		vertices[to].incoming_female(ind);
	}
}

/* record_path: edges can save information on the individuals going through.
 * --------------------------------------------------------------------------------------*/
void Graph::record_path ( Individual* ind, ent from, ent to) {
	ent index (nodes_to_edgesmask_map[std::make_pair (from, to)]);
	if (index) {
		tracked_paths.insert(index-1);
		edges[index-1].track_path (ind, from, to);
	}
}

/* exile_ind: send an individual out of the map (to a special vertex called 'out')
 * --------------------------------------------------------------------------------------*/
void Graph::exile_ind (Individual* ind) {
	ent from (ind->ask_location());
	if (ind->ask_gender()) {
		vertices[from].outgoing_male(ind);
		out.incoming_male(ind);
	} else {
		vertices[from].outgoing_female(ind);
		out.incoming_female(ind);
	}
}

/* ***************************************************************************************
 * Individual-interactivity methods
 * ***************************************************************************************/

/* print_graph: Returns two CSV files following the format used by 'igraph' R package
 * (Csardi & Nepusz 2006), so it can be useful for displaying the network and check that
 * everything works as expected.
 * --------------------------------------------------------------------------------------*/
void Graph::print_graph (const Population& pop) {

	std::string ext (".csv");
	std::string vertex_file ("vertex");
	std::string edge_file ("edges");
	static ent Nprints (0);

	// Check number of males and females per vertex
	iVector females_per_node;
	iVector males_per_node;
	Vector size_per_node;
	for (ent i(0); i<order; ++i) {
		females_per_node.push_back(0);
		males_per_node.push_back(0);
		size_per_node.push_back(0);
	}
	ent N (pop.inquire_N());
	ent Nfem (pop.inquire_Nladies());
	for (ent i(0); i<Nfem; ++i) {
		++females_per_node[pop.inquire_location(i)];
		if (pop.inquire_size(i) > size_per_node[pop.inquire_location(i)]) {
			size_per_node[pop.inquire_location(i)] = pop.inquire_size(i);
		}
	}
	for (ent i(Nfem); i<N; ++i) {
		++males_per_node[pop.inquire_location(i)];
		if (pop.inquire_size(i) > size_per_node[pop.inquire_location(i)]) {
			size_per_node[pop.inquire_location(i)] = pop.inquire_size(i);
		}
	}

	// Output vertex file
	std::ofstream out_vertex (vertex_file + std::to_string(Nprints) + ext);
	out_vertex << "id,females,males,size" << std::endl;
	for (ent i(0); i<order; ++i) {
		out_vertex << vertices[i].get_ID() << "," << females_per_node[i] << "," << males_per_node[i] << ',' << size_per_node[i] << std::endl;
	}

	// Output edges file
	std::ofstream out_edges (edge_file + std::to_string(Nprints)+ ext);
	out_edges << "from,to,width" << std::endl;
	for (const auto& edge: edges) {
		out_edges << edge.get_vertex1() << "," << edge.get_vertex2() << "," << edge.get_width() << std::endl;
	}

	++Nprints;
}

/* survey: Prints basic information about graph composition (for checking purposes).
 * --------------------------------------------------------------------------------------*/
void Graph::survey () {
	static ent count (0);
	std::cout << "Graph survey call " << ++count << std::endl;
	for (ent i(0); i<order; ++i) {
		std::cout << "vertex " << vertices[i].get_ID()
				  << " [" << vertices[i].get_coordinate().x
				  << "," << vertices[i].get_coordinate().y
				  << "] contains " << vertices[i].get_Nfem()
				  << " females and " << vertices[i].get_Nmal()
				  << " males"
				  << std::endl;
	}
}
