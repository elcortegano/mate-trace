#ifndef FEMALE_H
#define FEMALE_H

# include "individual.h"

class Female: public Individual {

protected:

	std::list<Individual*> spermathecal;
	static Gaussian dist_distance; // distribution of movement distance

public:

	// Constructor & destructor
	Female (ent n, const Genetic_parameters& gp) : Individual (n,gp) { gender = false; initialize_traits(gp); size.mask_P (gp.dimorphism.mean_fem); }
	Female (ent n, Individual* ind , const Deck2D& f , const Deck2D& g , const Genetic_parameters& gp, ent diff) :
	Individual (n,ind,f,g,gp,diff) { gender = false; size.mask_P (gp.dimorphism.mean_fem); }
	~Female () {};

	// Get methods
	bool ask_gender () const { return false; }
	ent ask_Nmates () const { return spermathecal.size(); }
	bool is_pregnant () { if (spermathecal.size()) return true; else return false; }
	std::list<Individual*> get_sperm () { return spermathecal; }

	// Network-interactivity
	iVector search_direction_from ( Graph& , ent );
	bool avail_destination ( ent , Graph& );
	void place ( Graph& , ent , ent );

	// Individual-interactivity
	void mate ( Individual* , Graph& );
	void marry ( Individual* , Graph& );

protected:

	// Get methods
	coefficient sample_distance () { return dist_distance(rnd_generator); }

	// Set methods
	void set_dist_mov ( const Genetic_parameters& gp) { dist_distance = Gaussian (gp.dist.mean_fem, gp.dist.sd_fem); }

};

#endif
