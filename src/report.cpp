/* =======================================================================================
 *                                                                                       *
 *       Filename: report.cpp                                                            *
 *                                                                                       *
 *    Description: Functions to save results of simulations and results in a format      *
 *    readable by R language.                                                            *
 *                                                                                       *
 ======================================================================================= */

#include "report.h"

/* report_graph: Saves a file with information on the graph used as world map
 * --------------------------------------------------------------------------------------*/
void report_graph (Graph& map, const word& prefix) {
	std::string filename ("graph.csv");
	if (prefix != EMPTY) filename = prefix + DELIM + filename;
	std::ofstream file;
	file.open(filename);
	file << "degree" << std::endl;

	for (ent i(0); i<map.get_order(); ++i) {
		file << map.get_vertex(i).degree() << std::endl;
	}
}
