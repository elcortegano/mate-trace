#ifndef ERROR_H
#define ERROR_H


// =====   HEADER FILE INCLUDES   =====

#include <iostream>
#include <vector>
#include <string>

// =====   INITIALIZED VARIABLES  =====

constexpr short	pe (96); // number of signs printed by pequal()


// =========   ERROR MESSAGES  ========
const std::string

	// Settings
	ERROR_SETNG_UNKNOWN = "(Settings) Unknown option",
	ERROR_SETNG_NONARGV = "(Settings) Non-option ARGV-elements",
	ERROR_SETNG_DEFPOPN = "(Settings) a population size parameter is mandatory (eg. -N 100). See mate-trace --help",
	ERROR_SETNG_DEFGENT = "(Settings) a number of generations or iterations is mandatory (eg. -t 100). See mate-trace --help",
	ERROR_SETNG_DEFMAPS = "(Settings) set a map size (eg. -V 100) or random encounters (-r). See mate-trace --help",
	ERROR_SETNG_DEFSEAS = "(Settings) at least one option for season ending is mandatory. See mate-trace --help",
	ERROR_SETNG_ZEROPOP = "(Settings) Population size must be higher than zero",
	ERROR_SETNG_NEGAFEC = "(Settings) Minimum fecundity cannot be negative",
	ERROR_SETNG_ZEROFEC = "(Settings) Fecundity should be set higher than zero",
	ERROR_SETNG_ZEROGEN = "(Settings) Number of generations must be higher than zero",
	ERROR_SETNG_TOPOOPT = "(Settings) Unknown graph model [option -T, --shape]",
	ERROR_SETNG_MOVEOPT = "(Settings) Unknown movement value [option -M, --movement]",
	ERROR_SETNG_MMCMOPT = "(Settings) Unknown male-male competence model [--mm-comp]",
	ERROR_SETNG_PREFOPT = "(Settings) Unknown preference model [option -P, --preference]",
	ERROR_SETNG_LNWDCHK = "(Settings) Map length and width options are restricted to 'rectangular-shaped' networks",
	ERROR_SETNG_RNDDIST = "(Settings) Distance-related parameters are disabled for random mating",
	ERROR_SETNG_MAPCOST = "(Settings) Male-cost only affects simulations using map, nor random mating",
	ERROR_SETNG_RNDMMAP = "(Settings) Map-related parameters are disabled for random mating",
	ERROR_SETNG_MATOMET = "(Settings) Cannot limit season durability both by total and successful (mating) encounters",
	ERROR_SETNG_DISTRIB = "(Settings) Failed to read distribution parameters [--dist* options]",
	ERROR_SETNG_DIMORPH = "(Settings) Cannot set generic and gender-specific parameters simultaneously",
	ERROR_SETNG_DICHOIC = "(Settings) Choice parameter is defined more than once",
	ERROR_SETNG_CHOIRNG = "(Settings) Choice parameter must be in the range [-1.0, 1.0]",
	ERROR_SETNG_BIASDIM = "(Settings) Cannot set different bias when both sexes choose",
	ERROR_SETNG_BIASSEX = "(Settings) Cannot set bias for females and males at once",
	ERROR_SETNG_SDPARAM = "(Settings) Standard deviation parameter cannot be negative",
	ERROR_SETNG_VARPARM = "(Settings) No variance parameter can be set negative",
	ERROR_SETNG_TOOCHOS = "(Settings) Multiple choosy options defined",
	ERROR_SETNG_HARDRND = "(Settings) Cannot use try-hard mating methods without enabling random mating",
	ERROR_SETNG_HARDMAX = "(Settings) Try-hard matings are incompatible with setting a maximum number of encounters or matings",
	ERROR_SETNG_HARDMON = "(Settings) Monogamy undefined for try-hard matings",
	ERROR_SETNG_HARDALL = "(Settings) Try-hard matings are undefined for both genders simultaneously",
	ERROR_SETNG_TRYREPL = "(Settings) There is no notion of sampling with/without replacement when using try-hard matings",
	ERROR_SETNG_OPT_EQU = "(Settings format) Attempt to set an option with multiple equal (=) signs",
	ERROR_SETNG_NUM_MIN = "(Settings format) Attempt to set a number with multiple minus sign",
	ERROR_SETNG_NUM_DOT = "(Settings format) Attempt to set a number with multiple dots",
	ERROR_SETNG_NUM_BDO = "(Settings format) Attempt to set a number begining with dot",
	ERROR_SETNG_NUM_EDO = "(Settings format) Attempt to set a number ending with dot",

	// Build network errors
	ERROR_GRAPH_FITSIZE = "Trying to build a network higher than specified by number of nodes",
	ERROR_GRAPH_BIGWIDT = "Graph width cannot exceed its length value",
	ERROR_GRAPH_ZRORDER = "Graph order is zero, increase map size",
	ERROR_GRAPH_BA_MINM = "Barabási-Albert networks require min(m0) = 2",
	ERROR_GRAPH_BA_LOWN = "Barabási-Albert networks require |V| >> m0",
	ERROR_GRAPH_BA_BADM = "Barabási-Albert networks require m <= m0",

	// Population composition
	ERROR_POPUL_SIZEMAP = "Population density too high. Reduce number of individuals or increase map size",

	// Individual-graph interactivity
	ERROR_INDIV_STAYMAP = "Current location must be always available",
	ERROR_INDIV_MOVEMAP = "Cannot find an available destination vertex",

	// Individual-individual interactivity
	ERROR_INDIV_NUMFEMA = "Unexpected number of females in destination vertex: no more than one female allowed",
	ERROR_INDIV_NUMMALE = "Unexpected number of males in destination vertex: no more than two males allowed",
	ERROR_INDIV_HOMOSEX = "Mating must happend between a female and a male (in that order)",
	ERROR_INDIV_NORAPES = "At least one individual excedeed the mating limit",
	ERROR_INDIV_NORIVAL = "Cannot compete against individuals who exceeded mating limit",
	ERROR_INDIV_MOMFEMA = "Mothers must be females!",
	ERROR_INDIV_MOMPREG = "Mothers must be pregnant!",
	ERROR_INDIV_NOMONOG = "Monogamy is impossible: too few females of males! Try balancing fecundity, the number of matings, and population size",

	// Mate choice parameters
	ERROR_MCHOI_MAXPREF = "Cannot calculate maximum preference value without a preference matrix",
	ERROR_MCHOI_UNKMMCM = "Unknown male-male competence model",
	ERROR_MCHOI_UNKPREF = "Unknown preference model",
	ERROR_MCHOI_CHOHARD = "Undefined choosy individuals for tryhard matings",

// =========   WARNING MESSAGES  ========

	// Conflictive settings
	WARNG_SETNG_TRYHARD = "Try hard mattings will force a season time of 1 round",
	WARNG_SETNG_RNDMMCP = "There is male-male competence for random matings. It will be ignored",

	// Population events
	WARNG_POPUL_EXTINCT = "Population has begone extinct due to low reproductive potential",

	// Individual-map interactivity
	WARNG_INDIV_TRAPPED = "Individual are being trapped in unavailable vertices. They will extra move to scape";


// ====   INLINE FUNCTIONS   ====

/* pequal: Prints a selected character a variable number of times.
 * --------------------------------------------------------------------------------------*/
inline void pequal (short n, char sign, bool error) {
	std::string s ("");
	for (short i(0); i<n; ++i) s += sign;
	s += "\n";
	if (error) std::cerr << s;
	else std::cout << s;
}

/* throw_error: Returns an error message, and exist the run of the program.
 * --------------------------------------------------------------------------------------*/
inline void throw_error (std::string message) {
	pequal(pe,'=', true); std::cerr << "ERROR: " << message << "." << std::endl; pequal(pe,'=', true);
	exit(-1);
}

/* ------------------------------------------------------------------------------------- */

inline void throw_error (std::string message, std::string tag) {
	pequal(pe,'=', true); std::cerr << "ERROR: " << message << " [ " << tag << " ]\n"; pequal(pe,'=', true);
	exit(-1);
}

/* throw_warning: Returns a warning message.
 * --------------------------------------------------------------------------------------*/
inline void throw_warning (std::string message) {
	static std::vector<std::string> outputed;
	bool print (true);
	for (auto& msg: outputed) {
		if (msg==message) {
			print = false;
			break;
		}
	}
	if (print)  {
		pequal(pe,'=', true); std::cerr << "WARNING: " << message << "." << std::endl; pequal(pe,'=', true);
		outputed.push_back(message);
	}
}

#endif
