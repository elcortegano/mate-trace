#ifndef SETTINGS_H
#define SETTINGS_H

#include <iostream>
#include <getopt.h>
#include <cstring>
#include <algorithm>
#include <fstream>
#include "alias.h"
#include "error.h"
#include "utils.h"


// =======   AVAILABLE OPTIONS  =======
static struct option long_options[] = {
	// *name, has_arg, *flag, val
	// general options
	{"help",		no_argument,		0,	'h'},
	{"name",		required_argument,	0,	'n'},
	{"debug",		no_argument,		0,	0},
	{"seed",		required_argument,	0,	's'},
	{"generations",	required_argument,	0,	't'},
	{"iterate",		no_argument,		0,	'i'},
	{"save-log",	no_argument,		0,	0},
	// map options
	{"shape",		required_argument,	0,	'T'},
	{"Map-size",	required_argument,	0,	'V'},
	{"length",		required_argument,	0,	'l'},
	{"width",		required_argument,	0,	'w'},
	// population options
	{"Pop-size",	required_argument,	0,	'N'},
	{"migrants",	required_argument,	0,	'm'},
	{"ompg",		no_argument,		0,	0},
	{"limit-rate",	required_argument,	0,	'R'},
	{"delta",		required_argument,	0,	'b'},
	{"d",			required_argument,	0,	'd'},
	{"aging",		required_argument,	0,	'a'},
	{"male-cost",	no_argument,		0,	0},
	{"season-time",	required_argument,	0,	0},
	{"n-matings",	required_argument,	0,	0},
	{"n-meetings",	required_argument,	0,	0},
	// biological traits
	{"Va",			required_argument,	0,	0},
	{"Ve",			required_argument,	0,	0},
	{"size",		required_argument,	0,	0},
	{"size.fem",	required_argument,	0,	0},
	{"size.mal",	required_argument,	0,	0},
	{"size.Va",		required_argument,	0,	0},
	{"size.Ve",		required_argument,	0,	0},
	{"fec.min",		required_argument,	0,	0},
	{"fec.k",		required_argument,	0,	0},
	{"fec.Ve",		required_argument,	0,	0},
	{"dimorphism",	required_argument,	0,	'D'},
	{"D.Va",		required_argument,	0,	0},
	{"D.Ve",		required_argument,	0,	0},
	{"movement",	required_argument,	0,	'M'},
	{"path-time",	required_argument,	0,	0},
	{"no-path",		no_argument,		0,	0},
	// mate choice
	{"choice",		required_argument,	0,	'C'},
	{"C.fem",		required_argument,	0,	0},
	{"C.mal",		required_argument,	0,	0},
	{"C.Va",		required_argument,	0,	0},
	{"C.Ve",		required_argument,	0,	0},
	{"bias",		required_argument,	0,	'B'},
	//{"B.fem",		required_argument,	0,	0},
	//{"B.mal",		required_argument,	0,	0},
	{"B.Va",		required_argument,	0,	0},
	{"B.Ve",		required_argument,	0,	0},
	{"choosy-males",no_argument,		0,	0},
	{"choosy-females", no_argument,		0,	0},
	{"tolerance",	required_argument,	0,	'S'},
	{"mm-comp",		required_argument,	0,	0},
	{"preference",	required_argument,	0,	'P'},
	{"monogamy",	no_argument,		0,	0},
	// random matings
	{"random",		no_argument,		0,	'r'},
	{"try-hard",	no_argument,		0,	0},
	// output
	{"save-meetings", no_argument, 	0,	0},
	{0,0,0,0}
};

const std::string OPT_DIST_MOV = "--dist.mov";
const std::string OPT_DIST_MOV_FEM = "--dist.mov.fem";
const std::string OPT_DIST_MOV_MAL = "--dist.mov.mal";

// ====   STRUCTS   ====

struct General_parameters {
	word name;
	bool debug;
	ent seed;
	ent t;
	bool iterate;
};

enum Shape {
	RECTANGULAR, // rectangular 'shaped'
	BA // Barabási-Albert network
};

struct Graph_parameters {
	Shape shape; // the 'shape' of the network
	ent N_vertices; // map size
	ent length; // map length
	ent width; // map width
	bool track_path; // enable edges for saving information
	ent path_time; // maximum time that path is saved on an edge
	ent m0; // initial number of vertices in the network (eg. required for BA model)
	ent m; // number of edges added each time step (eg. required for BA model)
};

enum Movement {
	ONE,
	PSEUDO_B
};

struct Population_parameters {
	Movement movement;
	ent aging; // simulate aging to sterility (and set minimum age)
	bool male_cost; // male mate cost (affects to movility)
	ent N_inds; // population size
	ent season_time; // maximum number of movements per mating season
	ent max_nmatings; // maximum number of successful matigns per season
	ent max_nmeetings; // maximum number of mettings produced per season
	ent R; // reproductive limiting rate
	coefficient migration; // migration rate
	bool monogamy; // set monogamic matings
};

enum MMC {
	SIZE,
	RANDOM,
};

enum PModel {
	GAUSSIAN_FND,
	GAUSSIAN_BD03,
	BINARY_05,
	FIRST_SIGHT
};

enum choosy_t {
	MALES,
	FEMALES,
	BOTH
};

struct Matechoice_parameters {
	coefficient S;
	MMC mm_competence;
	PModel preference;
	bool random_encounters;
	bool replacement; // in case of random encounters
	choosy_t choosy;
	bool thm;
};

struct Trait_parameters {
	coefficient Va;
	coefficient Ve;
	coefficient mean_fem;
	coefficient mean_mal;
	coefficient min;
	coefficient max;
};

struct Movement_parameters {
	coefficient mean_fem;
	coefficient mean_mal;
	coefficient sd_fem;
	coefficient sd_mal;
};

struct Genetic_parameters {
	coefficient delta; // inbreeding load
	coefficient d; // purging coefficient
	Trait_parameters choice;
	Trait_parameters bias;
	Trait_parameters size;
	Trait_parameters dimorphism;
	coefficient min_fecundity;
	coefficient k; // correlation fecundity / trait
	coefficient Ve_fecundity;
	Movement_parameters dist;
};

struct Output_parameters {
	bool save_meetings;
};


// =======   DEFAULT VALUES  =======
const word DEFAULT_NAME = EMPTY;
constexpr bool DEFAULT_DEBUG = false;
constexpr ent DEFAULT_T = 0;
constexpr bool DEFAULT_ITER = false;

constexpr ent DEFAULT_N = 0;
const Movement DEFAULT_MOVE_TYPE = PSEUDO_B;
constexpr coefficient DEFAULT_MIGRATION = 0.0;
constexpr bool DEFAULT_MONOGAMY = false;

constexpr Shape DEFAULT_SHAPE = RECTANGULAR;
constexpr ent DEFAULT_MAPSIZE = 0;
constexpr ent DEFAULT_MAP_LENGTH = 0;
constexpr ent DEFAULT_MAP_WIDTH = 0;
constexpr ent DEFAULT_M0 = 8;
constexpr ent DEFAULT_M = 8;

constexpr coefficient DEFAULT_MEAN_C = 0.0;
constexpr coefficient DEFAULT_SD_C = 0.0;
constexpr coefficient DEFAULT_MIN_C = 0.0;
constexpr coefficient DEFAULT_MAX_C = 1.0;
constexpr coefficient DEFAULT_MEAN_B = 0.0;
constexpr coefficient DEFAULT_SD_B = 0.0;
constexpr coefficient DEFAULT_MIN_B = 0.0;
constexpr coefficient DEFAULT_MAX_B = 10.0;
constexpr coefficient DEFAULT_S = 0.1;
constexpr MMC DEFAULT_MMCOMPETENCE = SIZE;
constexpr PModel DEFAULT_PREFERENCE = GAUSSIAN_FND;
constexpr bool DEFAULT_ENCOUNTERS = false;
constexpr bool DEFAULT_REPLACEMENT = true;
constexpr choosy_t DEFAULT_CHOOSY = BOTH;
constexpr bool DEFAULT_TRYHARD = false;

constexpr coefficient DEFAULT_VA = 1.0;
constexpr coefficient DEFAULT_VE = 1.0;
constexpr ent DEFAULT_R = 20;
constexpr coefficient DEFAULT_DELTA = 0.0;
constexpr coefficient DEFAULT_D = 0.0;
constexpr ent DEFAULT_AGING = 0;
constexpr bool DEFAULT_MALECOST = false;
constexpr ent DEFAULT_DURABILITY = 0;
constexpr bool DEFAULT_TRACKPATH = true;
constexpr ent DEFAULT_SEASON_TIME = 0;
constexpr ent DEFAULT_NMATINGS = 0;
constexpr ent DEFAULT_NMEETINGS = 0;

constexpr coefficient DEFAULT_MEAN_DIS = 3.0;
constexpr coefficient DEFAULT_SD_DIS = 1.0;
constexpr coefficient DEFAULT_MEAN_SIZE = 5.0;
constexpr coefficient DEFAULT_MIN_SIZE = 0.0;
constexpr coefficient DEFAULT_MAX_SIZE = 10.0;
constexpr coefficient DEFAULT_DIMOR = 0.0;
constexpr coefficient DEFAULT_VAR_DIMOR = 0.0;
constexpr coefficient DEFAULT_MIN_DIMOR = 0.0;
constexpr coefficient DEFAULT_MIN_FECUNDITY = 0.0;
constexpr coefficient DEFAULT_K = 1.0;

constexpr bool DEFAULT_SAVE_MEETINGS = false;


// ====   CLASSES   ====
class Settings {

protected:

	// General options
	General_parameters parameters;

	// Parameters
	Graph_parameters map;
	Population_parameters pop_info;
	Matechoice_parameters mc_info;
	Genetic_parameters genetics;
	Output_parameters output;

	// Control variables for complex options
	bool set_dist_mov;
	bool set_dist_mov_fem;
	bool set_dist_mov_mal;
	bool set_dist_size;
	bool set_dist_size_fem;
	bool set_dist_size_mal;
	bool set_dist_C;
	bool set_dist_C_fem;
	bool set_dist_C_mal;
	bool call_simple_C;
	bool call_gender_C;
	bool call_simple_B;
	//bool call_fem_B;
	//bool call_mal_B;
	bool call_simple_size;
	bool call_gender_size;

public:

	Settings ( int , char** );

	// Print
	void print_help ();

	// Get methods
	word get_name () { return parameters.name; }
	bool get_debug () { return parameters.debug; }
	ent get_seed () { return parameters.seed; }
	ent get_t () { return parameters.t; }
	ent get_iterate () { return parameters.iterate; }
	Graph_parameters get_graph_parameters () { return map; }
	Population_parameters get_pop_parameters () { return pop_info; }
	Matechoice_parameters get_mc_parameters () { return mc_info; }
	Genetic_parameters get_genetic_parameters () { return genetics; }
	Output_parameters get_output_parameters () { return output; }

protected:

	// Help
	std::string print_option ( std::string , std::string , bool , bool );
	std::string print_complex ( std::string , std::string , std::string );
	std::string print_example ( std::vector<std::string> );

	// Read specific options
	void parse_complex ( std::vector<std::string>& );
	void parse_dist_mov ( std::vector<std::string>& , ent& );
	void read_graph_shape ( std::string );
	void read_move_type ( std::string );
	void read_mm_comp (std::string );
	void read_preference (std::string );

	// Log
	void save_log ();

};


// =======   HELPER FUNCTIONS  =======

inline std::string print_movement (const Movement& move) {
	if (move==ONE) return "One";
	else return "Pseudo-brownian";
}

inline std::string print_shape (const Shape& shape) {
	if (shape==RECTANGULAR) return "Rectangular";
	else return "Barabási-Albert";
}

inline std::string print_mmc (const MMC& mmc) {
	if (mmc==SIZE) return "Size";
	else return "Random";
}

inline std::string print_pmodel (const PModel& pmodel) {
	if (pmodel==GAUSSIAN_FND) return "Gaussian FND";
	else if (pmodel==GAUSSIAN_BD03) return "Gaussian BD03";
	else if (pmodel==BINARY_05) return "Binary 05";
	else return "First sight";
}

#endif
