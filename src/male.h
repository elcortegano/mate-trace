#ifndef MALE_H
#define MALE_H

# include "individual.h"


class Male: public Individual {

protected:

	ent conquests;
	bool active; // actively looking for a partner
	static Gaussian dist_distance; // distribution of movement distance

public:

	// Constructor & destructor
	Male (ent n, const Genetic_parameters& gp) : Individual (n,gp) { gender = true; conquests = 0; initialize_traits(gp); active = true; }
	Male (ent n, Individual* ind , const Deck2D& f , const Deck2D& g , const Genetic_parameters& gp , ent diff) :
	Individual (n,ind,f,g,gp,diff) { gender = true; conquests = 0; active = true; }
	~Male () {};

	// Get methods
	bool ask_gender () const { return true; }
	ent ask_Nmates () const { return conquests; }
	std::list<Individual*> get_sperm () { throw_error(ERROR_INDIV_MOMFEMA); return {}; }

	// Network-interactivity
	iVector search_direction_from ( Graph& , ent );
	bool avail_destination ( ent , Graph& );
	void place ( Graph& , ent , ent );

	// Individual-interactivity
	void focus ();
	void add_conquest () { ++conquests; }

protected:

	// Get methods
	coefficient sample_distance () { return dist_distance(rnd_generator); }

	// Set methods
	void set_dist_mov ( const Genetic_parameters& gp) { dist_distance = Gaussian (gp.dist.mean_mal, gp.dist.sd_mal); }

};

#endif
