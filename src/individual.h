#ifndef INDIVIDUAL_H
#define INDIVIDUAL_H


#include <list>
#include "alias.h"
#include "error.h"
#include "settings.h"
#include "genetics.h"

// =====   GLOBAL, EXTERN & STATIC VARIABLES   =====

class Graph;
class Trait;

extern std::mt19937 rnd_generator;


// ====   CLASSES   ====
class Individual {

friend class Trait;

protected:

	ent name; // unique ID for the individual (required?)
	ent name_mom;
	ent name_dad;
	ent age; // measured as mating attempts
	bool gender; // 0 female, 1 male
	bool enough; // enough sex?
	Trait size; // body size
	Trait dimorphism; //  body size sexual dimorphism
	Fitness fecundity; // ability to generate offspring
	Trait C; // choice parameter
	Trait B; // bias parameter
	ent location; // the ID of the vertex where the individual is located
	ent previous_location; // idem, but in a previous iteration
	coefficient F; // inbreeding coefficient
	coefficient g; // purged inbreeding coefficient
	static Movement movement_type; // type of movement

public:

	// Constructor & destructor
	Individual (ent, const Genetic_parameters&);
	virtual ~Individual () {};
	Individual (ent, Individual*, const Deck2D&, const Deck2D&, const Genetic_parameters&, ent );

	// Get methods
	ent ask_name () const { return name; }
	ent ask_name_mom () const { return name_mom; }
	ent ask_name_dad () const { return name_dad; }
	virtual bool ask_gender () const =0;
	bool is_male () { return gender; }
	bool had_enough () { return enough; }
	coefficient ask_size () const { return size.value_P(); }
	coefficient gen_size () const { return size.value_A(); }
	coefficient ask_fecundity () const { return fecundity.value_P(); }
	coefficient ask_choice () const { 
		if (C.value_P() > 1.0) return 1.0;
		else if (C.value_P() < -1.0) return -1.0;
		else return C.value_P();
	}
	coefficient ask_bias () const { return B.value_P(); }
	coefficient ask_dimorphism () const { return dimorphism.value_P(); }
	coefficient gen_choice () const { return C.value_A(); }
	ent ask_location () const { return location; }
	coefficient ask_F () const { return F; }
	coefficient ask_g () const { return g; }
	virtual ent ask_Nmates () const { return 0; }
	ent ask_age () const { return age; }
	virtual bool is_pregnant () { return false; }

	// Set methods
	void rename ( ent n ) { name = n; }
	void set_move_type ( const Movement& );
	void set_min_sterile_age ( ent );

	// Network-interactivity
	virtual iVector search_direction_from ( Graph& , ent ) =0;
	iVector search_direction ( Graph& );
	virtual ent go_distance ();
	ent sample_destination ( const iVector& , Graph& );
	virtual bool avail_destination ( ent , Graph& ) =0;
	void move ( const iVector& , Graph& );
	void place ( Graph& , ent );
	virtual void place ( Graph& , ent , ent );
	void rest ( );
	void exit ( Graph& );

	// Individual-interactivity
	bool is_alone ( Graph& );
	virtual void focus () {}
	virtual void mate ( Individual* , Graph& ) {}
	virtual void marry ( Individual* , Graph& ) {}
	virtual void add_conquest () {}

	// Individual lifetime
	void aging ();

	// Overloaded operators
	friend bool operator< ( const Individual& , const Individual& );
	friend bool operator<= ( const Individual& , const Individual& );
	friend bool operator> ( const Individual& , const Individual& );
	friend bool operator>= ( const Individual& , const Individual& );

protected:

	// Get methods
	Trait trait_size () const { return size; }
	Trait trait_dimorphism () const { return dimorphism; }
	Trait trait_fecundity () const { return fecundity; }
	Trait trait_C () const { return C; }
	Trait trait_B () const { return B; }
	virtual coefficient sample_distance () =0;
	virtual std::list<Individual*> get_sperm () =0;

	// Set methods
	void initialize_traits ( const Genetic_parameters& );
	virtual void set_dist_mov( const Genetic_parameters& ) =0;

	// Individual-interactivity
	void done () { enough = true; }

};

#endif
