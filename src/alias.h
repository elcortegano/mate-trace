#ifndef ALIAS_H
#define ALIAS_H

#include <vector>
#include <list>
#include <deque>
#include <string>
#include <random>
#include <iostream>
#include <boost/math/distributions/normal.hpp>


// ============   ALIAS    ============

typedef double               coefficient;
typedef std::vector<coefficient>  Vector;
typedef std::vector<std::string> sVector;
typedef std::size_t ent                 ;
typedef std::vector<ent>         iVector;
typedef std::vector<bool>        bVector;
typedef std::vector<Vector>       Matrix;
typedef std::vector<sVector>     sMatrix;
typedef std::vector<iVector>     iMatrix;
typedef std::vector<bVector>     bMatrix;
typedef std::list<coefficient>      List;
typedef std::list<List>           List2D;
typedef std::deque<coefficient>     Deck;
typedef std::deque<Deck>          Deck2D;
typedef std::vector <Vector*>    pVector;
typedef std::vector <iVector*>  piVector;
typedef std::vector <Matrix*>    pMatrix;
typedef std::vector <iMatrix*>  piMatrix;
typedef std::string                 word;
typedef unsigned              individual;

typedef std::bernoulli_distribution Bernouilli;
typedef std::binomial_distribution<ent> Binomial;
typedef std::normal_distribution<coefficient> Gaussian;
typedef std::uniform_real_distribution<coefficient> real_Unif;
typedef std::uniform_int_distribution<ent> int_Unif;


// =============   ENUM    ============

enum Method {
	LR   = 1,  // LR method
	NNLR = 2,  // NNLR method
	D    = 3   // Estimate 'g' for a given 'd' value
};


// ===========   STRUCTS    ===========

// Coordinates [X,Y,Z] in a three-dimensional space
// They are defined from 1 (not 0) to N
struct Coordinate {
	ent x;
	ent y;

	bool operator==(const Coordinate& c) const {
		return x==c.x && y ==c.y;
	}
	bool operator<(const Coordinate& c) const {
		return x < c.x || (x==c.x && y < c.y);
	}
};


// ======   GLOBAL VARIABLES    ======
const word INT = "INT";
const word NUM = "NUM";
const word TYPE = "TYPE";
const word EMPTY = "";
const word TAB = "\t";
const word SPACE = " ";
const word DELIM = "_";

#endif
