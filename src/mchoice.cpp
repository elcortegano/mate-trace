/* =======================================================================================
 *                                                                                       *
 *       Filename: mchoice.cpp                                                           *
 *                                                                                       *
 *    Description: Defines a class that encapsulate parameters related to mate choice    *
 *                                                                                       *
 ======================================================================================= */

#include "mchoice.h"
#include "population.h"


/* ***************************************************************************************
 * Constructor & destructor
 * ***************************************************************************************/

/* MChoice: Initializes the suite of mating choice parameters.
 * --------------------------------------------------------------------------------------*/
MChoice::MChoice (Population& pop, const Matechoice_parameters& mc_info) {
	init (pop, mc_info);
}

/* init: This function initializes all basic parameters for mate choice. It is aimged to be
 * called once by mating season
 * --------------------------------------------------------------------------------------*/
void MChoice::init (Population& pop, const Matechoice_parameters& mc) {
	mc_info = mc;
	tolerance = mc.S;
	c_impatience = false;
	n_mates = 0;
	calc_f = true;
	calculate_Dmax(pop);
	if (mc_info.preference==BINARY_05 || mc_info.preference==FIRST_SIGHT) calc_f = false;
	if (calc_f) calculate_f_matrix (pop);
	this->update (pop);
}

/* update: Update some mate choice parameters (eg. parameters that depend only on available
 * individuals and need to be updated after every mating round
 * --------------------------------------------------------------------------------------*/
void MChoice::update (Population& pop) {
	calculate_nmates(pop);
	if (calc_f) calculate_fmax (pop);
}

/* ***************************************************************************************
 * Mate choice parameters
 * ***************************************************************************************/

/* calculate_nmates: Updates the number of mates in the population
 * --------------------------------------------------------------------------------------*/
void MChoice::calculate_nmates (Population& pop) {
	n_mates = 0;
	ent N_ladies (pop.inquire_Nladies());
	for (ent i(0); i<N_ladies; ++i) {
		n_mates += pop.get_ind(i)->ask_Nmates();
	}
}

/* calculate_Dmax: Calculates the maximum phenotypic difference
 * --------------------------------------------------------------------------------------*/
void MChoice::calculate_Dmax (Population& pop) {
	coefficient min_f (pop.get_ind(0)->ask_size());
	ent N (pop.inquire_N());
	ent N_ladies (pop.inquire_Nladies());
	coefficient max_f (min_f);
	for (ent i(1); i<N_ladies; ++i) {
		if (pop.get_ind(i)->ask_size()<min_f) min_f = pop.get_ind(i)->ask_size();
		else if (pop.get_ind(i)->ask_size()>max_f) max_f = pop.get_ind(i)->ask_size();
	}
	coefficient min_m (pop.get_ind(N_ladies)->ask_size());
	coefficient max_m (min_m);
	for (ent i(N_ladies+1); i<N; ++i) {
		if (pop.get_ind(i)->ask_size()<min_m) min_m = pop.get_ind(i)->ask_size();
		else if (pop.get_ind(i)->ask_size()>max_m) max_m = pop.get_ind(i)->ask_size();
	}

	if (abs(max_f-min_m) > abs(min_f-max_m)) D_max = abs(max_f-min_m);
	else D_max = abs(min_f-max_m);
}

/* ***************************************************************************************
 * Competence models
 * ***************************************************************************************/

/* mm_competence: Generic function for male-male competence.
 * --------------------------------------------------------------------------------------*/
std::list<Individual*>::iterator MChoice::mm_competence (std::list<Individual*> competitors) {

	// No competence
	if (competitors.size()==1) return competitors.begin();

	// Check competence conditions
	if (competitors.size()>2) throw_error (ERROR_INDIV_NUMMALE);
	for (auto& competitor: competitors) {
		if (competitor->had_enough()) throw_error(ERROR_INDIV_NORIVAL);
	}

	// Rank males by their competence value
	Vector ranks;
	if (mc_info.mm_competence==SIZE) ranks = comp_size(competitors);
	else if (mc_info.mm_competence==RANDOM) ranks = comp_random (competitors);
	else throw_error (ERROR_MCHOI_UNKMMCM);

	// Return iterator to the winner
	coefficient U = sample_prob();
	ent count (0);
	for (std::list<Individual*>::iterator it=competitors.begin(); it!=competitors.end(); ++it) {
		if (U<=ranks[count]) return it;
		++count;
	}
	return competitors.end();
}

/* comp_size: Male-male competence function that gives each individual a probability of
 * success proportional to its body size.
 * --------------------------------------------------------------------------------------*/
Vector MChoice::comp_size (std::list<Individual*> competitors) {
	coefficient sum_trait (0.0);
	for (auto& competitor: competitors) sum_trait += competitor->ask_size();
	Vector ranks;
	for (std::list<Individual*>::iterator it=competitors.begin(); it!=competitors.end(); ++it) {
		ranks.push_back((*it)->ask_size() / sum_trait);
		if (it!=competitors.begin()) ranks[ranks.size()-1] += ranks[ranks.size()-2];
	}
	return ranks;
}

/* comp_random: Male-male competence function that rank every individual equally.
 * --------------------------------------------------------------------------------------*/
Vector MChoice::comp_random (std::list<Individual*> competitors) {
	ent Nmales (competitors.size());
	Vector ranks;
	for (std::list<Individual*>::iterator it=competitors.begin(); it!=competitors.end(); ++it) {
		ranks.push_back(1.0/Nmales);
		if (it!=competitors.begin()) ranks[ranks.size()-1] += ranks[ranks.size()-2];
	}
	return ranks;
}

/* ***************************************************************************************
 * Preference models
 * ***************************************************************************************/

/* match: Calculates probability of mating given a preference vlaue
 * --------------------------------------------------------------------------------------*/
bool MChoice::match (Individual* female, Individual* male) {
	male->aging();
	if (f_max==0.0) return 1.0;
	coefficient preference;
	if (mc_info.preference==GAUSSIAN_FND || mc_info.preference==GAUSSIAN_BD03) {
		preference = calc_gaussian_preference(female,male);
	} else if (mc_info.preference==BINARY_05) {
		preference = 0.5*f_max;
	} else if (mc_info.preference==FIRST_SIGHT) {
		return true;
	} else {
		throw_error (ERROR_MCHOI_UNKPREF);
	}
	preference /= f_max;
	coefficient U = sample_prob();
	if (U<=preference) return true;
	else return false;
}

/* generic: General preference function that gives the propensity of mating between an
 * individuai i with another j
 * --------------------------------------------------------------------------------------*/
coefficient MChoice::generic (coefficient alpha0, coefficient alpha1, coefficient Z_ij) {
	return exp(alpha0 + alpha1*Z_ij);
}

/* calc_gaussian_preference: Template for gaussian preference models
 * --------------------------------------------------------------------------------------*/
coefficient MChoice::calc_gaussian_preference ( Individual* i, Individual* j ) {
	coefficient alpha0 (0.0);
	coefficient s (tolerance);
	coefficient C (j->ask_choice()); // choosy males
	coefficient B (j->ask_bias());
	if ((C < i->ask_choice() && mc_info.choosy==BOTH) || mc_info.choosy==FEMALES) C = i->ask_choice();
	if (mc_info.choosy==FEMALES) B = i->ask_bias();
	// B is temporally set as B(mal) when mc_info.choosy==BOTH

	// Compute impatience factor
	if (c_impatience) s *= (1.0 + n_mates);

	// Calculating alpha1
	coefficient alpha1 (0.0);
	if (mc_info.preference==GAUSSIAN_FND) {
		alpha1 = alpha_FND (C,s);
	} else if (mc_info.preference==GAUSSIAN_BD03) {
		alpha1 = alpha_BD03 (C,s);
	} else {
		throw_error (ERROR_MCHOI_UNKPREF);
	}

	// Calculating Z_ij
	coefficient D_ij (abs(i->ask_size() - j->ask_size()));
	if (mc_info.choosy != BOTH) D_ij = i->ask_size() - j->ask_size();
	if (C<0.0) B = D_max; // assortative negative mating
	coefficient Z_ij ((D_ij-B)*(D_ij-B));
	if (mc_info.preference==GAUSSIAN_FND) Z_ij /= (D_max*D_max);
	else if (mc_info.preference==GAUSSIAN_BD03) Z_ij /= 2.0;

	return generic (alpha0, alpha1, Z_ij);
}

/* alpha_FND: Regression term alpha1 in FND gaussian model
 * --------------------------------------------------------------------------------------*/
coefficient MChoice::alpha_FND (coefficient C, coefficient s) {
	return (-pow(C/s,2.0));
}

/* alpha_BD03: Regression term alpha1 in BD03 gaussian model
 * --------------------------------------------------------------------------------------*/
coefficient MChoice::alpha_BD03 (coefficient C, coefficient s) {
	return (-pow(pow(C,2.0)/s,2.0));
}

/* calculate_f_matrix: Calculates the matrix of preference values
 * --------------------------------------------------------------------------------------*/
void MChoice::calculate_f_matrix (Population& pop) {
	ent N (pop.inquire_N());
	ent N_ladies (pop.inquire_Nladies());
	f_matrix.clear();
	for (ent i(0); i<N_ladies; ++i) {
		Vector f_row;
		for (ent j(N_ladies); j<N; ++j) {
			f_row.push_back(calc_gaussian_preference(pop.get_ind(i),pop.get_ind(j)));
		}
		f_matrix.push_back(f_row);
	}
}

/* calculate_fmax: Calculates the maximum preference value among all available pairs of
 * indviduals
 * --------------------------------------------------------------------------------------*/
void MChoice::calculate_fmax (Population& pop) {
	ent N_ladies (pop.inquire_Nladies());
	if (!f_matrix.size() || (f_matrix.size() != N_ladies)) throw_error (ERROR_MCHOI_MAXPREF);
	f_max = 0.0;
	for (ent i(0); i<f_matrix.size(); ++i) {
		if (!pop.get_ind(i)->had_enough()) {
			for (ent j(0); j<f_matrix[i].size(); ++j) {
				if (!pop.get_ind(j+N_ladies)->had_enough()) {
					if (f_matrix[i][j]>f_max) f_max = f_matrix[i][j];
				}
			}
		}
	}
}
