/* =======================================================================================
 *                                                                                       *
 *       Filename: individual.cpp                                                        *
 *                                                                                       *
 *    Description: Defines the individuals that will move and interact in the graph      *
 *                                                                                       *
 ======================================================================================= */

#include "individual.h"
#include "graph.h"
#include "genetics.h"
#include "utils.h"

/* ***************************************************************************************
 * Constructor & destructor
 * ***************************************************************************************/

/* Individual: Initializes an individual with random parameters.
 * --------------------------------------------------------------------------------------*/
Individual::Individual (ent n, const Genetic_parameters& genetics) {

	name = n;
	name_mom = 0;
	name_dad = 0;
	age = 0;
	enough = false;
	F = 0.0;
	g = F;

}

/* Individual (mom): Initializes an individual from information of its parents.
 * --------------------------------------------------------------------------------------*/
Individual::Individual (ent n, Individual* mom, const Deck2D& coancestry, const Deck2D& purged_coancestry, const Genetic_parameters& genetics, ent diff) {

	if (mom->ask_gender()) throw_error (ERROR_INDIV_MOMFEMA);
	if (!mom->is_pregnant()) throw_error (ERROR_INDIV_MOMPREG);

	ent index (sample_from_zero_to(mom->ask_Nmates()-1));
	std::list<Individual*>::iterator it = std::next(mom->get_sperm().begin(), index);

	name = n;
	name_mom = mom->ask_name();
	name_dad = (*it)->ask_name();
	age = 0;
	enough = false;
	ent R (diff+1); // scale index

	if (!name_mom || !name_dad) F = 0.0;
	else if (name_mom >= name_dad) F = coancestry[name_mom-R][name_dad-R];
	else F = coancestry[name_dad-R][name_mom-R];
	if (genetics.d > 0.0) {
		if (!name_mom || !name_dad) g = 0.0;
		else if (name_mom >= name_dad) g = purged_coancestry[name_mom-R][name_dad-R];
		else g = purged_coancestry[name_dad-R][name_mom-R];
	} else g = F;

	// Inherite biological traits
	size = Trait (mom->trait_size(), (*it)->trait_size(), mom->ask_F(), (*it)->ask_F(), genetics.size);
	dimorphism = Trait (mom->trait_dimorphism(), (*it)->trait_dimorphism(), mom->ask_F(), (*it)->ask_F(), genetics.dimorphism);
	fecundity = Fitness (size, genetics, g);
	C = Trait (mom->trait_C(), (*it)->trait_C(), mom->ask_F(), (*it)->ask_F(), genetics.choice);
	B = Trait (mom->trait_B(), (*it)->trait_B(), mom->ask_F(), (*it)->ask_F(), genetics.bias);

}

/* set_move_type: Initialize static attribute movement_type
 * --------------------------------------------------------------------------------------*/
Movement Individual::movement_type = DEFAULT_MOVE_TYPE;

void Individual::set_move_type (const Movement& move) {
	movement_type = move;
}

/* initialize_traits: Initialize the genetic and phenotypic value of biological traits
 * --------------------------------------------------------------------------------------*/
void Individual::initialize_traits (const Genetic_parameters& gp) {

	// Set movement distribution
	set_dist_mov(gp);

	// Body size (additive)
	size = Trait (gp.size, gender);
	dimorphism = Trait (gp.dimorphism, gender);

	// Fecundity (non-additive, fitness)
	fecundity = Fitness (size, gp, g);

	// Choice parameters (additive)
	C = Trait (gp.choice, gender);
	B = Trait (gp.bias, gender);

}

/* ***************************************************************************************
 * Network-interactivity
 * ***************************************************************************************/

/* search_direction: Returns a vector of candidate paths, based on the ID of the neighbour
 * vertices
 * --------------------------------------------------------------------------------------*/
iVector Individual::search_direction (Graph& map) {
	// Search from current location
	return search_direction_from(map, location);
}

/* search_destination: samples a destination vertex from all candidates, and then checks if
 * its available, if so, that destination is returned, otherwise it searches the closes one
 * that is.
 * --------------------------------------------------------------------------------------*/
ent Individual::sample_destination (const iVector& paths, Graph& map) {
	if (!paths.size()) return location;
	else  if (paths.size()==1) return paths[0];
	else {
		ent try_path (sample(paths));
		return try_path;
	}
}

/* go_distance: Calculates the distance (ie. number of vertices) the individual moves in a
 * row
 * --------------------------------------------------------------------------------------*/
ent Individual::go_distance () {
	if (movement_type==ONE) return 1;
	else {
		coefficient dis = sample_distance();
		if (dis <= 0.0) return 0;
		else return std::round (dis);
	}
}

/* move: Move individual in the map.
 * --------------------------------------------------------------------------------------*/
void Individual::move (const iVector& itinerary, Graph& map) {
	if (itinerary.size() > 1) {
		ent it_index(0);
		for (ent i(itinerary.size()-1); i>=0; --i) {
			if (this->avail_destination(itinerary[i],map)) {
				it_index = i;
				break;
			}
		}
		for (ent i(0); i<it_index; ++i) this->place(map, itinerary[i], itinerary[i+1]);
	} else {
		this->rest();
	}
}

/* place: Change location of an individual in the map.
 * --------------------------------------------------------------------------------------*/
void Individual::place (Graph& map, ent new_location) {
	location = new_location;
	previous_location = location;
	map.move_ind(this, new_location);
}

void Individual::place (Graph& map, ent old_location, ent new_location) {
	location = new_location;
	previous_location = old_location;
	map.move_ind(this, old_location, new_location);
}

/* rest: Individuals skip movement
 * --------------------------------------------------------------------------------------*/
void Individual::rest () {
	previous_location = location;
}

/* exit: The individual ends its lifetime in the population, and exit the map
 * --------------------------------------------------------------------------------------*/
void Individual::exit (Graph& map) {
	this->done();
	map.exile_ind(this);
}

/* ***************************************************************************************
 * Individual-interactivity
 * ***************************************************************************************/

/* is_alone: Checks if an individual is alone in its position.
 * --------------------------------------------------------------------------------------*/
bool Individual::is_alone ( Graph& map) {

	std::size_t companions (0);
	if (map.get_vertex(this->ask_location()).get_Nfem() > 1) throw_error(ERROR_INDIV_NUMFEMA);
	if (map.get_vertex(this->ask_location()).get_Nmal() > 2) throw_error(ERROR_INDIV_NUMMALE);
	companions += map.get_vertex(this->ask_location()).get_Nfem();
	companions += map.get_vertex(this->ask_location()).get_Nmal();
	--companions;
	if (companions) return false;
	else return true;
}

/* ***************************************************************************************
 * Individual lifetime
 * ***************************************************************************************/

/* aging: Aging is measured as matings attempts until season ends. Every time an individual
 * grows up it have a chance to become sterile. The implementation below corresponds to
 * Carvajal-Rodríguez (2018) survival function. See Population::retirement()
 * --------------------------------------------------------------------------------------*/
void Individual::aging () {
	++age;
}

/* ***************************************************************************************
 * Overloaded operators
 * ***************************************************************************************/
bool operator< ( const Individual& self, const Individual& other) {
	return (self.ask_size() < other.ask_size());
}

bool operator<= ( const Individual& self, const Individual& other) {
	return !(other < self);
}

bool operator> ( const Individual& self, const Individual& other) {
	return other < self;
}

bool operator>= ( const Individual& self, const Individual& other) {
	return !(self < other);
}
