#ifndef VERTEX_H
#define VERTEX_H


#include <list>
#include "alias.h"
#include "individual.h"


// ====   CLASSES   ====
class Vertex {

protected:

	ent ID; // the unique identity of the vertex
	iVector pointed_vertices; // a list of the connected vertex (their IDs)
	iVector pointed_edges; // a list of the connected edges (their IDs)
	Coordinate coord; // cartesian coordinates of the vertex
	bool available; // is the vertex available?
	std::list<Individual*> females; // ID of inhabitant females
	std::list<Individual*> males; // idem for males
	ent capacity; // maximum allowed number of individuals

public:

	// Constructor & destructor
	Vertex () {}
	Vertex ( ent );
	Vertex ( ent , iVector );

	// Methods to calculate graph parameters
	ent degree () const { return pointed_edges.size(); };

	// Graph-interactivity methods
	void connect ( ent , ent );

	// Individual-interactivity methods
	bool is_available () const { return available; };
	void incoming_female( Individual* );
	void incoming_male( Individual* );
	void outgoing_female( Individual* );
	void outgoing_male( Individual* );

	// Get methods
	ent get_ID () const { return ID; };
	std::list<Individual*> get_fem () { return females; };
	std::list<Individual*> get_mal () { return males; };
	ent get_Nfem () const {	return females.size(); };
	ent get_Nmal () const {	return males.size(); };
	iVector get_pointed_vertices() const { return pointed_vertices; };
	iVector get_pointed_edges() const { return pointed_edges; };
	Coordinate get_coordinate () const { return coord; };

	// Set methods
	void set_coord ( Coordinate c ) { coord = c; };
	void reset () { available = true; females.clear(); males.clear(); };

};

#endif
