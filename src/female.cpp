/* =======================================================================================
 *                                                                                       *
 *       Filename: female.cpp                                                            *
 *                                                                                       *
 *    Description: Defines the methods that are specific to female individuals           *
 *                                                                                       *
 ======================================================================================= */

#include "female.h"
#include "genetics.h"
#include "graph.h"

/* ***************************************************************************************
 * Constructor & destructor
 * ***************************************************************************************/

/* set_dist_*: Initialize static attributes related to distribution of traits
 * --------------------------------------------------------------------------------------*/
Gaussian Female::dist_distance = Gaussian (DEFAULT_MEAN_DIS, DEFAULT_SD_DIS);


/* ***************************************************************************************
 * Network-interactivity
 * ***************************************************************************************/

/* search_direction: Returns a vector of candidate paths, based on the ID of the neighbour
 * vertices
 * --------------------------------------------------------------------------------------*/
iVector Female::search_direction_from (Graph& map, ent place) {

	// Get destination vertices
	std::vector<Vertex> ng_vertices (map.get_neighbour_vertices(place));

	// Females does not have preference for any direction
	iVector path;
	for (const auto& vertex: ng_vertices) {
		path.push_back(vertex.get_ID());
	}

	return path;
}

/* avail_destination: checks wether a destination vertex is available or not
 * --------------------------------------------------------------------------------------*/
bool Female::avail_destination (ent dest, Graph& map) {
    if (!map.get_vertex(dest).get_Nfem()) return true;
    else if (dest==location) return true;
    else return false;
}

/* place: Change location of an individual in the map.
 * --------------------------------------------------------------------------------------*/
void Female::place (Graph& map, ent old_location, ent new_location) {
	Individual::place(map, old_location, new_location);
	map.record_path (this, old_location, new_location);
}

/* ***************************************************************************************
 * Individual-interactivity
 * ***************************************************************************************/

/* mate: Female is inseminated by a male.
 * --------------------------------------------------------------------------------------*/
void Female::mate (Individual* male, Graph& map) {
	if (this->ask_gender() || (!male->ask_gender())) throw_error (ERROR_INDIV_HOMOSEX);
	if (this->had_enough() || male->had_enough()) throw_error (ERROR_INDIV_NORAPES);
	male->add_conquest();
	spermathecal.push_back(male);
	this->aging();
}

void Female::marry ( Individual* male, Graph& map) { // monogamic mating
	this->Female::mate(male,map);
	this->exit(map);
	male->exit(map);
}
