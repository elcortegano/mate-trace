/* =======================================================================================
 *                                                                                       *
 *       Filename:  main.cpp                                                             *
 *                                                                                       *
 *    Description:  A software for the analysis and simulation of matting choice of      *
 *                  individuals under different conditions of intra and inter-gender     *
 *                  competence.                                                          *
 *                                                                                       *
 *                                                                                       *
 *        Version:  1.alpha.0                                                            *
 *        Created:  28/06/2018                                                           *
 *        Updated:  --/--/----                                                           *
 *       Compiler:  g++ (8.1.1)                                                          *
 *                                                                                       *
 *         Author:  Eugenio López-Cortegano                                              *
 *          Email:  e.lopez@uvigo.es                                                     *
 *   Organization:  Universidad de Vigo                                                  *
 *                                                                                       *
 ======================================================================================= */


#include "main.h"

int main (int argc, char** argv) {

	// LET THERE BE LIGHT
	const clock_t begin_time = clock();
	Settings settings (argc, argv);
	word name (settings.get_name());
	rnd_generator.seed (settings.get_seed());
	std::cout << "seed: " << settings.get_seed() << std::endl;

	// GET SETTING PARAMETERS
	bool debug_mode (settings.get_debug());
	Graph_parameters graph_info (settings.get_graph_parameters());
	Population_parameters pop_info (settings.get_pop_parameters());
	ent generations (settings.get_t()+1);
	bool iterate (settings.get_iterate());
	Matechoice_parameters mc (settings.get_mc_parameters ());
	Genetic_parameters genetics (settings.get_genetic_parameters());
	Output_parameters output (settings.get_output_parameters());
	bool random_mating (mc.random_encounters);
	bool tryhard_mating (mc.thm);

	// BUILD MAP
	if (!mc.random_encounters) std::cout << "\rBuilding map...";
	Graph world (graph_info);
	if (!mc.random_encounters) {
		std::cout << "\rBuilding map...done!" << std::endl;
		report_graph(world, name);
	}

	// SUMMON INDIVIDUALS
	std::cout << "Running simulation..." << std::endl;
	Population population (pop_info, world, genetics, mc, output);
	if (debug_mode) { population.survey(); world.survey(); }
	population.log_individuals();

	// SIMULATION OVER t MATING SEASONS
	if (!random_mating) {
		do {
			do {
				population.move_individuals(world);
				if (debug_mode) { population.survey(); world.survey(); }
				population.mate_individuals(world);
				world.clear_old_paths();
			} while (!population.season_ends(world, generations, name, iterate));
		} while (generations);
	} else if (!tryhard_mating) {
		do {
			do {
				if (debug_mode) { population.survey(); world.survey(); }
				population.random_mating(world, mc.replacement);
			} while(!population.season_ends(world, generations, name, iterate));
		} while (generations);
	} else {
		do {
			do {
				if (debug_mode) { population.survey(); world.survey(); }
				population.tryhard_mating (world);
			} while (!population.season_ends(world, generations, name, iterate));
		} while (generations);
	}

	// THE END
	std::cout << "Done! Execution took " << float( clock () - begin_time ) /  CLOCKS_PER_SEC << " seconds." << std::endl;

	return 0;
}
