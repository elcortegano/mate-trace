/* =======================================================================================
 *                                                                                       *
 *       Filename: male.cpp                                                              *
 *                                                                                       *
 *    Description: Defines the methods that are specific to male individuals             *
 *                                                                                       *
 ======================================================================================= */

#include "male.h"
#include "genetics.h"
#include "graph.h"

/* ***************************************************************************************
 * Constructor & destructor
 * ***************************************************************************************/

/* set_dist_*: Initialize static attributes related to distribution of traits
 * --------------------------------------------------------------------------------------*/
Gaussian Male::dist_distance = Gaussian (DEFAULT_MEAN_DIS, DEFAULT_SD_DIS);


/* ***************************************************************************************
 * Network-interactivity
 * ***************************************************************************************/

/* search_direction: Returns a vector of candidate paths, based on the ID of the neighbour
 * vertices
 * --------------------------------------------------------------------------------------*/
iVector Male::search_direction_from (Graph& map, ent place) {

	iVector path;

	// Males movement depend on their status ('active' attribute)
	if (!active) { // feeding mode (move as females)
		std::vector<Vertex> ng_vertices (map.get_neighbour_vertices(place));
		for (const auto& vertex: ng_vertices) path.push_back(vertex.get_ID());

	} else { // mating mode

		// Males look for outgoing edges with size > 0
		std::vector<Edge> tmp_edges;
		std::vector<Edge> ng_edges (map.get_neighbour_edges(place));
		for (ent i(0); i<ng_edges.size(); ++i) {
			if (ng_edges[i].get_width() > 0.0) {
				if (ng_edges[i].get_vertex1() == place) {
					tmp_edges.push_back(ng_edges[i]);
				}
			}
		}

		// If there are no female paths close, any direction is likely
		if (!tmp_edges.size()) tmp_edges = ng_edges;

		// Get destination vertices
		for (ent i(0); i<tmp_edges.size(); ++i) {
			if (tmp_edges[i].get_vertex1() == place) {
				path.push_back(map.get_vertex(tmp_edges[i].get_vertex2()).get_ID());
			} else {
				path.push_back(map.get_vertex(tmp_edges[i].get_vertex1()).get_ID());
			}
		}
	}

	return path;
}

/* avail_destination: checks wether a destination vertex is available or not
 * --------------------------------------------------------------------------------------*/
bool Male::avail_destination (ent dest, Graph& map) {
    if (map.get_vertex(dest).get_Nmal()<2) return true;
    else if (dest==location) return true;
    else return false;
}

/* place: Change location of an individual in the map.
 * --------------------------------------------------------------------------------------*/
void Male::place (Graph& map, ent old_location, ent new_location) {
	Individual::place(map, old_location, new_location);
}

/* ***************************************************************************************
 * Individual-interactivity
 * ***************************************************************************************/

/* focus: Determine the movement status, depending on their aging. A male can "focus" on
 * mating (active=true) or feeding. This function should only be called if aging is enabled.
 * --------------------------------------------------------------------------------------*/
void Male::focus () {
	if (!age) {
		active = true;
		return;
	}
	coefficient U (sample_prob());
	coefficient ratio (1.0/(age+1.0));
	if (U<ratio) active = true; // mating mode
	else active = false; // feeding mode
}
