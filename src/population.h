#ifndef POPULATION_H
#define POPULATION_H

#include <algorithm>
#include "alias.h"
#include "utils.h"
#include "individual.h"
#include "female.h"
#include "male.h"
#include "graph.h"
#include "settings.h"
#include "mchoice.h"


// =====   GLOBAL, EXTERN & STATIC VARIABLES   =====
extern std::mt19937 rnd_generator;
static ent ids = 0;

class Graph;


// ====  STRUCTS   ====
struct Pedigree_record {
	ent ind;
	ent mom;
	ent dad;
	coefficient size;
	coefficient F;
	coefficient g;
	ent t;
};
typedef std::deque<Pedigree_record> Pedigree;

struct Registry {
	ent ID_female;
	ent ID_male;
	coefficient P_female;
	coefficient P_male;
	coefficient C;
	ent t;
	bool success;
};


// ====  INLINE FUNCTIONS   ====
inline Pedigree_record ind_to_ped (Individual* ind, coefficient d, ent t) {
	Pedigree_record ped;
	ped.ind = ind->ask_name();
	ped.mom = ind->ask_name_mom();
	ped.dad = ind->ask_name_dad();
	ped.size = ind->ask_size();
	ped.F = ind->ask_F();
	if (d > 0.0) ped.g = ind->ask_g();
	ped.t = t;
	return ped;
}

// ====   CLASSES   ====
class Population {

protected:

	ent generation;
	std::vector<Individual*> population;
	ent N;
	ent max_N;
	ent N_ladies;
	Pedigree pedigree; // genealogical records of the parents of current population
	ent n_old_members;
	std::vector<Registry> registry;
	ent nmatings;
	Deck2D coancestry;
	Deck2D purged_coancestry;
	Population_parameters parameters;
	Genetic_parameters pop_genetics;
	Output_parameters output;
	Survival survival_dist;
	ent n_migrants;

	// Mate choice parameters
	MChoice pop_mchoice;
	choosy_t choosy;

public:

	// Constructor & destructor
	Population ( const Population_parameters& , Graph& , const Genetic_parameters& , const Matechoice_parameters& , const Output_parameters& );
	~Population ();

	// Individuals activity
	void spread_individuals ( Graph& );
	void move_individuals ( Graph& );
	void mate_individuals ( Graph& );
	void random_mating ( Graph& , bool );
	void tryhard_mating ( Graph& );

	// Environmental methods
	bool season_ends ( Graph& , ent& , const std::string& , bool );

	// Get methods
	Individual* get_ind ( ent index ) { return population[index]; }
	ent inquire_N () const { return N; }
	ent inquire_Nladies () const { return N_ladies; }
	ent inquire_location ( ent ind ) const { return population[ind]->ask_location(); }
	ent inquire_size ( ent ind ) const { return population[ind]->ask_size(); }

	// Set methods
	void log_individuals ();
	void log_matings ( Individual* , Individual* , bool );
	void clear_pedigree ();

	// Save methods
	void survey ();
	void save_pedigree ( const word& );
	void save_registry ( const word& , bool );

protected:

	// Individuals activity
	iVector contributions ();
	void birth ( std::vector<Female>& , std::vector<Male>& , const iVector& );
	void random_population ( std::vector<Individual*>& );
	void retirement ( Graph& );

	// Environmental methods
	bool extinction ();
	void mutation ();
	void mutation ( Trait_parameters& );

	// Set methods
	void update_inbreeding ();
	void set_new_population ( const std::vector<Individual*>& , ent );
};

#endif
