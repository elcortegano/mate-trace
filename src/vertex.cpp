/* =======================================================================================
 *                                                                                       *
 *       Filename: vertex.cpp                                                            *
 *                                                                                       *
 *    Description: Defines the properties of vertex (ie. nodes) of the graph.            *
 *                 In other words, it represents each discrete location in the space     *
 *                                                                                       *
 ======================================================================================= */

#include "vertex.h"

/* ********************************************************************************************
 * Vertex::Vertex ()
 * --------------------------------------------------------------------------------------------
 * Class Vertex constructor
 * ********************************************************************************************/
Vertex::Vertex (ent unique_ID) :
	ID (unique_ID),
	available (true),
	capacity (2) {

}

Vertex::Vertex ( ent unique_ID, iVector v_IDs) :
	Vertex (unique_ID) {

	pointed_vertices = v_IDs;

}

/* ********************************************************************************************
 * Vertex::connect ()
 * --------------------------------------------------------------------------------------------
 * Created a conection with another vertex
 * ********************************************************************************************/
void Vertex::connect ( ent vertex_ID, ent edge_ID) {
	pointed_vertices.push_back(vertex_ID);
	pointed_edges.push_back(edge_ID);
}

/* ********************************************************************************************
 * Vertex::SET methods ()
 * --------------------------------------------------------------------------------------------
 * List of methods to modify protected attributes
 * ********************************************************************************************/
void Vertex::incoming_female (Individual* id) {
	females.push_back(id);
}

void Vertex::incoming_male(Individual* id) {
	males.push_back(id);
}

void Vertex::outgoing_female(Individual* id) {
	females.remove(id);
}

void Vertex::outgoing_male(Individual* id) {
	males.remove(id);
}
