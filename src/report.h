#ifndef REPORT_H
#define REPORT_H

#include "alias.h"
#include "graph.h"
#include "population.h"

// =====   GLOBAL, EXTERN & STATIC VARIABLES   =====
extern std::mt19937 rnd_generator;

// =====   FUNCTION'S PROTOTYPES   =====
void report_graph ( Graph& , const word& );


#endif
