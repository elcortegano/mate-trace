#ifndef GRAPH_H
#define GRAPH_H


#include <fstream>
#include <math.h>
#include <map>
#include <unordered_set>
#include <utility>
#include "alias.h"
#include "error.h"
#include "utils.h"
#include "vertex.h"
#include "edge.h"
#include "individual.h"
#include "population.h"


// =====   GLOBAL, EXTERN & STATIC VARIABLES   =====
extern std::mt19937 rnd_generator;
class Population;


// ====   CLASSES   ====
class Graph { // G = (V,E)

protected:

	ent order; // The order ( |V| ) of a graph is the number of vertices
	ent size; // The size ( |E| ) of a graph is the number of edges
	Shape appearance; // The shape of the network
	std::vector<Vertex> vertices; // A list of vertex, in order of appearance in 'network'
	Vertex out; // A special vertex that is 'out of the map'
	std::vector<Edge> edges; // A list of all edges involded
	std::map<std::pair<ent,ent>,ent> nodes_to_edgesmask_map; // A map of pairs of vertices to edges mask (edge ID +1)
	std::map<Coordinate,ent> coordinates_to_vertexmask_map; // A map of coordinates to a vertices mask (vertex ID +1)
	std::unordered_set<ent> tracked_paths; // A map of the edges that keep information on them
	iVector degree_seq; // The degree sequence

public:

	// Constructor & destructor
	Graph ( const Graph_parameters& );
	~Graph ();

	// Get methods
	ent get_order () const  { return order; };
	Vertex get_vertex ( ent index ) const { return vertices[index]; };
	Edge get_edge ( ent index ) { return edges[index]; };
	std::vector<Vertex> get_neighbour_vertices ( ent ) const;
	std::vector<Edge> get_neighbour_edges ( ent ) const;
	ent map_vertexmask_from_coordinates ( Coordinate& coord) { return coordinates_to_vertexmask_map[coord]; };
	ent map_vertex_from_coordinates ( Coordinate& coord) { return map_vertexmask_from_coordinates(coord)-1; };

	// Methods to calculate graph parameters
	void degree_sequence ();
	ent max_degree () { return degree_seq[0]; }; // Δ(G)
	ent min_degree () { return degree_seq[degree_seq.size()-1]; }; // δ(G)
	coefficient P ( ent ) const; // P(k), the degree distribution

	// Individual-interactivity methods
	void move_ind ( Individual* , ent );
	void move_ind ( Individual* , ent , ent );
	void record_path ( Individual* , ent , ent );
	void exile_ind ( Individual* );

	// Set methods
	void clear_path () { for (auto& edge: edges) edge.reset_width(); };
	void clear_old_paths () {
		std::unordered_set<ent>::iterator it = tracked_paths.begin();
		while (it != tracked_paths.end()) {
			if (edges[*it].clear_old_path()) it = tracked_paths.erase(it);
			else ++it;
		}
	}
	void clear () {
		for (auto& vertex: vertices) vertex.reset();
		this->clear_path();
	}

	// Print methods
	void print_graph ( const Population& );
	void survey ();

protected:

	// Building
	void connect ( ent , ent );
	void build_square ( ent , ent );
	void build_square ();
	void build_BA ( ent , ent );
	void BA_step ( ent , ent , Vector& );
	iVector check_neighbour_nodes ( ent i ) const { return vertices[i].get_pointed_vertices(); }
	iVector check_neighbour_edges ( ent i ) const { return vertices[i].get_pointed_edges(); }

};

#endif
