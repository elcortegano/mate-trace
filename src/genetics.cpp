/* =======================================================================================
 *                                                                                       *
 *       Filename: genetics.cpp                                                          *
 *                                                                                       *
 *    Description: Functions to define rules of inheritance and inbreeding.              *
 *                 Note that the program assumes non-overlapping generations.            *
 *                                                                                       *
 ======================================================================================= */

#include "genetics.h"
#include "individual.h"
#include "population.h"


/* ***************************************************************************************
 * Constructor & destructor
 * ***************************************************************************************/
/* Trait: Initializes a trait from probability distributions.
 * --------------------------------------------------------------------------------------*/
Trait::Trait (const Trait_parameters& t, bool gender) {
	// Genetic value
	if (gender) dist = Gaussian (t.mean_mal, sqrt(t.Va));
	else dist = Gaussian (t.mean_fem, sqrt(t.Va));
	A = dist (rnd_generator);
	compute_genotype ();
	// Environmental deviation
	E_E = DEFAULT_EE;
	// Phenotype
	compute_phenotype (sqrt(t.Ve));
}

/* Trait: Initializes a trait from inheritance of two parentals
 * --------------------------------------------------------------------------------------*/
Trait::Trait ( const Trait& mom_trait, const Trait& dad_trait, coefficient mom_F, coefficient dad_F, const Trait_parameters& t) {
	// Genetic value
	inherite_additive (mom_trait, dad_trait, mom_F, dad_F, t.Va);
	compute_genotype ();
	// Environmental deviation
	E_E = DEFAULT_EE;
	// Phenotype
	compute_phenotype (sqrt(t.Ve));
}

/* Fitness: Initializes a fitness trait partially correlated to an additive trait
 * The additive value is determined following the expression:
 * A(fec) = min(fec) + k · A(trait)
 * Where min(fec)=0 andk=1 by default, so that A(fec) = A(trait)
 * --------------------------------------------------------------------------------------*/
Fitness::Fitness (const Trait& trait, const Genetic_parameters& gp, coefficient g) {
	// Genetic value
	A = gp.min_fecundity + gp.k * trait.value_A ();
	compute_genotype (gp.delta, g);
	// Environmental deviation
	E_E = DEFAULT_EE;
	// Phenotype
	compute_phenotype (sqrt(gp.Ve_fecundity));
}

/* Fitness: Initializes a fitness trait from inheritance of two parentals
 * --------------------------------------------------------------------------------------*/
Fitness::Fitness ( const Trait& mom_trait, const Trait& dad_trait, coefficient mom_F, coefficient dad_F, const Genetic_parameters& gp, coefficient g) {
	// Genetic value
	inherite_additive (mom_trait, dad_trait, mom_F, dad_F, gp.size.Va);
	A = gp.min_fecundity + gp.k * A;
	compute_genotype (gp.delta, g);
	// Environmental deviation
	E_E = DEFAULT_EE;
	// Phenotype
	compute_phenotype (sqrt(gp.Ve_fecundity));
}

/* ***************************************************************************************
 * Inheritance methods
 * ***************************************************************************************/
/* inherite_additive: additive trait is inherited from that of the parents
* --------------------------------------------------------------------------------------*/
void Trait::inherite_additive (const Trait& mom_trait, const Trait& dad_trait, coefficient mom_F, coefficient dad_F, coefficient Va) {
	coefficient Ea (0.5 * (mom_trait.value_A() + dad_trait.value_A()));
	Va *= (1.0 - 0.5*(mom_F + dad_F));
	Gaussian progeny_dist (Ea, sqrt(Va));
	A = progeny_dist (rnd_generator);
}

/* initialize_coancestry: Initializes the coancestry matrix
 * -------------------------------------------------------------------------------------------*/
Deck2D initialize_coancestry (ent N) {
	Deck2D f;
	for (ent i(0); i<N; ++i) {
		Deck f_row;
		for (ent j(0); j<i+1; ++j) {
			if (i==j) f_row.push_back(0.5);
			else f_row.push_back(0.0);
		}
		f.push_back(f_row);
	}
	return f;
}

/* f_AB: Calculates the coancestry coefficient of an individual A with another individual B.
 * This coancestry is equal to the average coancestry of A's parents with B. The accesory
 * coancestry matrix is empty for standard coancestry (d=0) and the standard coancestry matrix
 * for purging analysis.
 * -------------------------------------------------------------------------------------------*/
coefficient g_AB (Individual* ind_A, const Pedigree_record& ped, const Deck2D& coancestry, coefficient d, ent diff) {

	ent R (diff+1); // scale index
	ent A (ind_A->ask_name());
	ent B (ped.ind);
	ent C (ind_A->ask_name_mom());
	ent D (ind_A->ask_name_dad());
	coefficient purging_term (1.0);

	// Self coancestry
	if (A == B) {
		if (!C || !D) return 0.5;
		if (d>0.0) purging_term = 1.0 - 2.0*d*(ind_A->ask_F());
		return 0.5*(1.0+coancestry[D-R][C-R])*purging_term;
	}

	// Coancestry
	if (!C && !D) return 0.0;
	else if (d>0.0 && B) {

		// Exact purged coancestry for non overlapping generations
		ent H (ped.mom);
		ent E (ped.dad);
		coefficient f_CH (0.0);
		coefficient f_CE (0.0);
		coefficient f_DH (0.0);
		coefficient f_DE (0.0);

		if (C) {
			if (H && C>H) f_CH = coancestry[C-R][H-R];
			else if (H) f_CH = coancestry[H-R][C-R];
			if (E && C>E) f_CE = coancestry[C-R][E-R];
			else if (E) f_CE = coancestry[E-R][C-R];
		}
		if (D) {
			if (H && D>H) f_DH = coancestry[D-R][H-R];
			else if (H) f_DH = coancestry[H-R][D-R];
			if (E && D>E) f_DE = coancestry[D-R][E-R];
			else if (E) f_DE = coancestry[E-R][D-R];
		}
		purging_term = 1.0 - d*(ind_A->ask_F()+ped.F);
		return 0.25*(f_CH+f_DH+f_CE+f_DE)*purging_term;

	} else if (B) {
		coefficient f_CB (0.0);
		coefficient f_DB (0.0);
		if (C && (B>C)) f_CB = coancestry[B-R][C-R];
		else if (C) f_CB = coancestry[C-R][B-R];
		if (D && (B>D)) f_DB = coancestry[B-R][D-R];
		else if (D) f_DB = coancestry[D-R][B-R];
		return 0.5*(f_CB + f_DB);
	} else return 0.0;

}

coefficient f_AB ( Individual* ind_A, const Pedigree_record& ped, const Deck2D& coancestry, ent diff) {
	return g_AB(ind_A, ped, coancestry, 0.0, diff);
}

/* sim_survival: Simulates whether an individual with phenotype P survives to adulthood or not.
 * survival is based on the same initial distribution of additive values and variance for 
 * body size.
 * -------------------------------------------------------------------------------------------*/
bool Survival::sim_survival (const Individual& i) {
	coefficient D = 0.0;
	if (!i.ask_gender()) D = i.ask_dimorphism();
	coefficient p = psurvival(i.ask_size()-D); // since the survival distribution is given by males, female dimorphism must be deviated
	coefficient x = sample_prob();
	if (p >= x) return true;
	else return false;
}
