/* =======================================================================================
 *                                                                                       *
 *       Filename: settings.cpp                                                          *
 *                                                                                       *
 *    Description: Functions to parse and set program options.                           *
 *                                                                                       *
 ======================================================================================= */

#include "settings.h"

/* ***************************************************************************************
 * Constructor & destructor
 * ***************************************************************************************/
Settings::Settings (int argc, char** argv) {

	// Set init default values
	parameters.seed = (int)time(0);
	parameters.name = DEFAULT_NAME;
	parameters.debug = DEFAULT_DEBUG;
	parameters.t = DEFAULT_T;
	parameters.iterate = DEFAULT_ITER;
	pop_info.aging = DEFAULT_AGING;
	pop_info.male_cost = DEFAULT_MALECOST;
	pop_info.N_inds = DEFAULT_N;
	pop_info.movement = DEFAULT_MOVE_TYPE;
	pop_info.migration = DEFAULT_MIGRATION;
	pop_info.monogamy = DEFAULT_MONOGAMY;
	pop_info.season_time = DEFAULT_SEASON_TIME;
	pop_info.max_nmatings = DEFAULT_NMATINGS;
	pop_info.max_nmeetings = DEFAULT_NMEETINGS;
	pop_info.R = DEFAULT_R;
	mc_info.S = DEFAULT_S;
	mc_info.mm_competence = DEFAULT_MMCOMPETENCE;
	mc_info.preference = DEFAULT_PREFERENCE;
	mc_info.random_encounters = DEFAULT_ENCOUNTERS;
	mc_info.replacement = DEFAULT_REPLACEMENT;
	mc_info.choosy = DEFAULT_CHOOSY;
	mc_info.thm = DEFAULT_TRYHARD;
	map.shape = DEFAULT_SHAPE;
	map.N_vertices = DEFAULT_MAPSIZE;
	map.length = DEFAULT_MAP_LENGTH;
	map.width = DEFAULT_MAP_WIDTH;
	map.path_time = DEFAULT_DURABILITY;
	map.track_path = DEFAULT_TRACKPATH;
	map.m0 = DEFAULT_M0;
	map.m = DEFAULT_M;
	genetics.delta = DEFAULT_DELTA;
	genetics.d = DEFAULT_D;
	genetics.size.Va = DEFAULT_VA;
	genetics.size.Ve = DEFAULT_VE;
	genetics.size.mean_fem = DEFAULT_MEAN_SIZE;
	genetics.size.mean_mal = DEFAULT_MEAN_SIZE;
	genetics.size.min = DEFAULT_MIN_SIZE;
	genetics.size.max = DEFAULT_MAX_SIZE;
	genetics.min_fecundity = DEFAULT_MIN_FECUNDITY;
	genetics.k = DEFAULT_K;
	genetics.Ve_fecundity = DEFAULT_VE;
	genetics.dimorphism.Va = DEFAULT_VAR_DIMOR;
	genetics.dimorphism.Ve = DEFAULT_VAR_DIMOR;
	genetics.dimorphism.mean_fem = DEFAULT_DIMOR;
	genetics.dimorphism.mean_mal = DEFAULT_DIMOR;
	genetics.dimorphism.min = DEFAULT_DIMOR;
	genetics.dimorphism.max = DEFAULT_DIMOR;
	genetics.dist.mean_fem = DEFAULT_MEAN_DIS;
	genetics.dist.mean_mal = DEFAULT_MEAN_DIS;
	genetics.dist.sd_fem = DEFAULT_SD_DIS;
	genetics.dist.sd_mal = DEFAULT_SD_DIS;
	genetics.choice.Va = DEFAULT_SD_C;
	genetics.choice.Ve = DEFAULT_SD_C;
	genetics.choice.mean_fem = DEFAULT_MEAN_C;
	genetics.choice.mean_mal = DEFAULT_MEAN_C;
	genetics.choice.min = DEFAULT_MIN_C;
	genetics.choice.max = DEFAULT_MAX_C;
	genetics.bias.Va = DEFAULT_SD_B;
	genetics.bias.Ve = DEFAULT_SD_B;
	genetics.bias.mean_fem = DEFAULT_MEAN_B;
	genetics.bias.mean_mal = DEFAULT_MEAN_B;
	genetics.bias.min = DEFAULT_MIN_B;
	genetics.bias.max = DEFAULT_MAX_B;
	output.save_meetings = DEFAULT_SAVE_MEETINGS;

	// Set control variables
	set_dist_mov = false;
	set_dist_mov_fem = false;
	set_dist_mov_mal = false;
	set_dist_size = false;
	set_dist_size_fem = false;
	set_dist_size_mal = false;
	set_dist_C = false;
	set_dist_C_fem = false;
	set_dist_C_mal = false;
	call_simple_C = false;
	call_gender_C = false;
	call_simple_C = false;
	call_simple_B = false;
	//call_fem_B = false;
	//call_mal_B = false;
	call_simple_size = false;
	call_gender_size = false;
	bool call_log (false);

	// Parse complex arguments
	std::vector<std::string> arguments;
	for (int i(0); i<argc; ++i) arguments.push_back(std::string(argv[i]));
	parse_complex (arguments);

	int new_argc (arguments.size());
	char ** new_argv = new char* [new_argc];
	for (int i(0); i<new_argc; ++i) {
		new_argv[i] = new char [arguments[i].size()+1];
		strcpy(new_argv[i], arguments[i].c_str());
	}

	// Temporary variables
	bool set_ompg (false);

	// Local variables
	int c;

	while (true) {

		int option_index (0);
		c = getopt_long (new_argc, new_argv, "hn:s:t:iT:N:M:m:D:C:S:b:V:l:w:P:rR:B:d:a:", long_options, &option_index);

		if (c == -1) break;

		switch (c) {

			case 'h':
			print_help ();

			case 'n':
			parameters.name = optarg;
			break;

			case 's':
			parameters.seed = atoi(optarg);
			break;

			case 't':
			parameters.t = atoi(optarg);
			break;

			case 'i':
			parameters.iterate = true;
			break;

			case 'T':
			read_graph_shape(optarg);
			break;

			case 'M':
			read_move_type(optarg);
			break;

			case 'm':
			pop_info.migration = atof(optarg);
			break;

			case 'D':
			genetics.dimorphism.mean_fem = atof(optarg);
			genetics.dimorphism.mean_mal = genetics.dimorphism.mean_fem;
			break;

			case 'C':
			if (call_gender_C) throw_error (ERROR_SETNG_DIMORPH);
			genetics.choice.mean_fem = atof(optarg);
			genetics.choice.mean_mal = genetics.choice.mean_fem;
			call_simple_C = true;
			break;

			case 'S':
			mc_info.S = atof(optarg);
			genetics.choice.Ve = mc_info.S;
			break;

			case 'B':
			//if (call_fem_B | call_mal_B) throw_error (ERROR_SETNG_DIMORPH);
			genetics.bias.mean_fem = atof(optarg);
			genetics.bias.mean_mal = genetics.bias.mean_fem;
			call_simple_B = true;
			break;

			case 'N':
			pop_info.N_inds = atoi(optarg);
			break;

			case 'V':
			map.N_vertices = atoi(optarg);
			break;

			case 'l':
			map.length = atoi(optarg);
			break;

			case 'w':
			map.width = atoi(optarg);
			break;

			case 'P':
			read_preference(optarg);
			break;

			case 'r':
			mc_info.random_encounters = true;
			break;

			case 'R':
			pop_info.R = atoi(optarg);
			break;

			case 'b':
			genetics.delta = atof(optarg);
			break;

			case 'd':
			genetics.d = atof(optarg);
			break;

			case 'a':
			pop_info.aging = atoi(optarg);
			break;

			case 0:
			if (long_options[option_index].flag != 0) break;
			else if (strcmp(long_options[option_index].name, "debug") == 0) parameters.debug = true;
			else if (strcmp(long_options[option_index].name, "save-log") == 0) call_log = true;
			else if (strcmp(long_options[option_index].name, "monogamy") == 0) pop_info.monogamy = true;
			else if (strcmp(long_options[option_index].name, "no-path") == 0) map.track_path = false;
			else if (strcmp(long_options[option_index].name, "ompg") == 0) set_ompg = true;
			else if (strcmp(long_options[option_index].name, "save-meetings") == 0) output.save_meetings = true;
			else if (strcmp(long_options[option_index].name, "C.fem") == 0) {
				if (call_simple_C) throw_error (ERROR_SETNG_DIMORPH);
				genetics.choice.mean_fem = atof(optarg);
				call_gender_C = true;
			} else if (strcmp(long_options[option_index].name, "C.mal") == 0) {
				if (call_simple_C) throw_error (ERROR_SETNG_DIMORPH);
				genetics.choice.mean_mal = atof(optarg);
				call_gender_C = true;
			} else if (strcmp(long_options[option_index].name, "C.Va") == 0) genetics.choice.Va = atof(optarg);
			else if (strcmp(long_options[option_index].name, "C.Ve") == 0) genetics.choice.Ve = atof(optarg);
			/*else if (strcmp(long_options[option_index].name, "B.fem") == 0) {
				if (call_simple_B) throw_error (ERROR_SETNG_DIMORPH);
				else if (call_mal_B) throw_error (ERROR_SETNG_BIASSEX);
				genetics.bias.mean_fem = atof(optarg);
				call_fem_B = true;
			} else if (strcmp(long_options[option_index].name, "B.mal") == 0) {
				if (call_simple_B) throw_error (ERROR_SETNG_DIMORPH);
				else if (call_fem_B) throw_error (ERROR_SETNG_BIASSEX);
				genetics.bias.mean_mal = atof(optarg);
				call_mal_B = true;*/
			else if (strcmp(long_options[option_index].name, "B.Va") == 0) genetics.bias.Va = atof(optarg);
			else if (strcmp(long_options[option_index].name, "B.Ve") == 0) genetics.bias.Ve = atof(optarg);
			else if (strcmp(long_options[option_index].name, "mm-comp") == 0) read_mm_comp(optarg);
			else if (strcmp(long_options[option_index].name, "choosy-males") == 0) {
				if (mc_info.choosy != DEFAULT_CHOOSY) throw_error (ERROR_SETNG_TOOCHOS);
				mc_info.choosy = MALES;
			} else if (strcmp(long_options[option_index].name, "choosy-females") == 0) {
				if (mc_info.choosy != DEFAULT_CHOOSY) throw_error (ERROR_SETNG_TOOCHOS);
				mc_info.choosy = FEMALES;
			} else if (strcmp(long_options[option_index].name, "try-hard") == 0) mc_info.thm = true;
			else if (strcmp(long_options[option_index].name, "path-time") == 0) map.path_time = atoi(optarg);
			else if (strcmp(long_options[option_index].name, "male-cost") == 0) pop_info.male_cost = true;
			else if (strcmp(long_options[option_index].name, "season-time") == 0) pop_info.season_time = atoi(optarg);
			else if (strcmp(long_options[option_index].name, "n-matings") == 0) pop_info.max_nmatings = atoi(optarg);
			else if (strcmp(long_options[option_index].name, "n-meetings") == 0) pop_info.max_nmeetings = atoi(optarg);
			else if (strcmp(long_options[option_index].name, "Va") == 0) {
				genetics.size.Va = atof(optarg);
				genetics.choice.Va = atof(optarg);
			} else if (strcmp(long_options[option_index].name, "Ve") == 0) {
				genetics.size.Ve = atof(optarg);
				genetics.choice.Ve = atof(optarg);
			} else if (strcmp(long_options[option_index].name, "size") == 0) {
				if (call_gender_size) throw_error (ERROR_SETNG_DIMORPH);
				genetics.size.mean_fem = atof(optarg);
				genetics.size.mean_mal = genetics.size.mean_fem;
				call_simple_size = true;
			} else if (strcmp(long_options[option_index].name, "size.fem") == 0) {
				if (call_simple_size) throw_error (ERROR_SETNG_DIMORPH);
				genetics.size.mean_fem = atof(optarg);
				call_gender_size = true;
			} else if (strcmp(long_options[option_index].name, "size.mal") == 0) {
				if (call_simple_size) throw_error (ERROR_SETNG_DIMORPH);
				genetics.size.mean_mal = atof(optarg);
				call_gender_size = true;
			} else if (strcmp(long_options[option_index].name, "size.Va") == 0) genetics.size.Va = atof(optarg);
			else if (strcmp(long_options[option_index].name, "size.Ve") == 0) genetics.size.Ve = atof(optarg);
			else if (strcmp(long_options[option_index].name, "D.Va") == 0) genetics.dimorphism.Va = atof(optarg);
			else if (strcmp(long_options[option_index].name, "D.Ve") == 0) genetics.dimorphism.Ve = atof(optarg);
			else if (strcmp(long_options[option_index].name, "fec.min") == 0) genetics.min_fecundity = atof(optarg);
			else if (strcmp(long_options[option_index].name, "fec.k") == 0) genetics.k = atof(optarg);
			else if (strcmp(long_options[option_index].name, "fec.Ve") == 0) genetics.Ve_fecundity = atof(optarg);
			break;

			case '?':
			throw_error (ERROR_SETNG_UNKNOWN);

			default:
			throw_error (ERROR_SETNG_UNKNOWN);

		}
	}

	// Error for mandatory options
	if (pop_info.N_inds==DEFAULT_N) throw_error(ERROR_SETNG_DEFPOPN);
	else if (parameters.t==DEFAULT_T) throw_error(ERROR_SETNG_DEFGENT);
	else if (map.N_vertices==DEFAULT_MAPSIZE && mc_info.random_encounters==DEFAULT_ENCOUNTERS && map.length==DEFAULT_MAP_LENGTH && map.width==DEFAULT_MAP_WIDTH) throw_error(ERROR_SETNG_DEFMAPS);
	else if (pop_info.season_time==DEFAULT_SEASON_TIME && pop_info.max_nmatings==DEFAULT_NMATINGS && pop_info.max_nmeetings==DEFAULT_NMEETINGS) throw_error(ERROR_SETNG_DEFSEAS);
	else if (pop_info.N_inds<0) throw_error(ERROR_SETNG_ZEROPOP);
	else if (parameters.t<0) throw_error(ERROR_SETNG_ZEROGEN);

	// Error for non-option arguments
	if (optind < new_argc) {
		std::string extra_args (new_argv[optind++]);
		while (optind < new_argc) {
			extra_args += " ";
			extra_args += (new_argv[optind++]);
		}
		throw_error(ERROR_SETNG_NONARGV, extra_args);
	}

	// Check incompatibilities
	if (map.shape!=RECTANGULAR) {
		if (map.length != DEFAULT_MAP_LENGTH || map.width != DEFAULT_MAP_WIDTH) {
			throw_error(ERROR_SETNG_LNWDCHK);
		}
	}
	else if (mc_info.random_encounters && (set_dist_mov | set_dist_mov_fem | set_dist_mov_mal)) throw_error(ERROR_SETNG_RNDDIST);
	else if (mc_info.random_encounters & pop_info.male_cost) throw_error (ERROR_SETNG_MAPCOST);
	else if ((mc_info.choosy == BOTH) && (call_simple_B || genetics.bias.Va!=DEFAULT_SD_B || genetics.bias.Ve!=DEFAULT_SD_B)) throw_error (ERROR_SETNG_BIASDIM);

	// Conditional default values
	if (parameters.t==0) throw_error (ERROR_SETNG_ZEROGEN);
	if (pop_info.season_time==0) pop_info.season_time = map.N_vertices;
	if (pop_info.monogamy) pop_info.aging = 1;
	if (set_ompg) pop_info.migration = 1.0/pop_info.N_inds;
	if (!map.track_path) map.path_time = 1;
	if (mc_info.random_encounters) pop_info.aging = 0;
	if (mc_info.random_encounters & pop_info.monogamy) mc_info.replacement = false;
	if (mc_info.random_encounters) {
		if (map.shape != DEFAULT_SHAPE) throw_error(ERROR_SETNG_RNDMMAP);
		else if (map.N_vertices != DEFAULT_MAPSIZE) throw_error(ERROR_SETNG_RNDMMAP);
		else if (map.length != DEFAULT_MAP_LENGTH) throw_error(ERROR_SETNG_RNDMMAP);
		else if (map.width != DEFAULT_MAP_WIDTH) throw_error(ERROR_SETNG_RNDMMAP);
		else if (map.path_time != DEFAULT_DURABILITY) throw_error(ERROR_SETNG_RNDMMAP);
		else if (map.track_path != DEFAULT_TRACKPATH) throw_error(ERROR_SETNG_RNDMMAP);
		else if (map.m0 != DEFAULT_M0) throw_error(ERROR_SETNG_RNDMMAP);
		else if (map.m != DEFAULT_M) throw_error(ERROR_SETNG_RNDMMAP);
		if (mc_info.mm_competence != DEFAULT_MMCOMPETENCE) throw_warning (WARNG_SETNG_RNDMMCP);
		map.N_vertices = pop_info.N_inds;
	}
	if (mc_info.thm) {
		if (!mc_info.random_encounters) throw_error (ERROR_SETNG_HARDRND);
		if (pop_info.max_nmatings != DEFAULT_NMATINGS || pop_info.max_nmeetings != DEFAULT_NMEETINGS) throw_error (ERROR_SETNG_HARDMAX);
		else if (mc_info.replacement != DEFAULT_REPLACEMENT) throw_error (ERROR_SETNG_TRYREPL);
		else if (pop_info.monogamy) throw_error (ERROR_SETNG_HARDMON);
		else if (mc_info.choosy == BOTH) throw_error (ERROR_SETNG_HARDALL);
		pop_info.season_time = 1;
		throw_warning (WARNG_SETNG_TRYHARD);
	}
	if (pop_info.max_nmatings != DEFAULT_NMATINGS && pop_info.max_nmeetings != DEFAULT_NMEETINGS) throw_error (ERROR_SETNG_MATOMET);
	map.m0 = map.m;
	if (genetics.choice.mean_fem < -1.0 || genetics.choice.mean_mal < -1.0) throw_error (ERROR_SETNG_CHOIRNG);
	if (genetics.choice.mean_fem > 1.0 || genetics.choice.mean_mal > 1.0) throw_error (ERROR_SETNG_CHOIRNG);
	if (genetics.dist.sd_fem < 0.0 || genetics.dist.sd_mal < 0.0) throw_error(ERROR_SETNG_SDPARAM);
	if (genetics.min_fecundity < 0.0) throw_error(ERROR_SETNG_NEGAFEC);
	if (genetics.k < 0.0 || (genetics.k==0.0 && genetics.min_fecundity==0)) throw_error(ERROR_SETNG_ZEROFEC);
	if (genetics.Ve_fecundity < 0.0 || genetics.size.Va < 0.0 || genetics.size.Ve < 0.0 || genetics.choice.Va < 0.0 || genetics.choice.Ve < 0.0 || genetics.bias.Va < 0.0 || genetics.bias.Ve < 0.0 || genetics.dimorphism.Va < 0.0 || genetics.dimorphism.Ve < 0.0)  throw_error(ERROR_SETNG_VARPARM);

	for (int i (0); i < new_argc; i++) delete[] new_argv[i];
	delete[] new_argv;
	if (call_log) save_log();
}

/* ***************************************************************************************
 * Print
 * ***************************************************************************************/

/* print_help: Returns a short summary of the program options
 * --------------------------------------------------------------------------------------*/
void Settings::print_help () {

	std::cout << "SYNOPSIS:" << std::endl;
	std::cout << TAB << "mate-trace" << SPACE << " -N -t [-V|-r] [--season-time] [--n-meetings|--n-matings]" << SPACE << "[OPTION]..." << std::endl;
	std::cout << std::endl;
	std::cout << TAB << "- eg: mate-trace -N 100 -t 1000 -V 1000 --season-time=100" << std::endl;
	std::cout << std::endl;
	std::cout << "DESCRIPTION:" << SPACE << "A software for the simulation and analysis of mate choice strategies using spatial\ninformation." << std::endl;
	std::cout << std::endl;
	std::cout << "OVERVIEW: Mate-trace has been built as a tool for the simulation of biological populations and" << std::endl;
	std::cout << "the investigation of mate choice strategies. Individuals in mate-trace can find partners at" << std::endl;
	std::cout << "random according to their phenotypic frequencies, but they can also move in a scenario where" << std::endl;
	std::cout << "rules for spatial information can be defined. The software also provides a complete set of" << std::endl;
	std::cout << "functions for preference, competence and inheritance." << std::endl;
	std::cout << std::endl;
	std::cout << "Find a more complete description of the software and the simulation steps in the README file" << std::endl;
	std::cout << "and the site's wiki (https://gitlab.com/elcortegano/mate-trace/wikis/home)." << std::endl;
	std::cout << std::endl;
	std::cout << "OPTIONS:" << std::endl;

	std::cout << std::endl << "  GENERAL OPTIONS" << std::endl;
	std::cout << print_option ("\t\tDisplay this help and exit", EMPTY,true, true) << std::endl;
	std::cout << print_option ("\t\tGives a name to the analysis run, that will be used as prefix for all output files (with '_' as delimiter)", EMPTY, true, true) << std::endl;
	std::cout << print_option ("\t\tEnables flags with information useful for debugging", EMPTY, false, true) << std::endl;
	std::cout << print_option ("\tSets a seed integer value for pseudo-random number generation\n\t\t\t\t(default is current time)", INT, true, true) << std::endl;
	std::cout << print_option ("The number of generations to simulate (default is 5)", INT, true, true) << std::endl;
	std::cout << print_option ("\t\tRuns <t> replicates of the base population instead of\n\t\t\t\tsimulating <t> generations", EMPTY, true, true) << std::endl;
	std::cout << print_option ("\t\tSaves a log file that records the options used", EMPTY, false, true) << std::endl;

	std::cout << std::endl << "  MAP OPTIONS" << std::endl;
	std::cout << print_option ("\tThe algorithm to build the map network. Valid types are\n\t\t\t\trectangular (`r`) and Barabási-Albert (`ba`). Default is `r`", TYPE, true, true) << std::endl;
	std::cout << print_option ("\tGraph order (total number of vertices). Default is 10.", INT, true, true) << std::endl;
	std::cout << print_option ("\tLength in a \"rectangular shaped\" graph", INT, true, true) << std::endl;
	std::cout << print_option ("\tWidth in a \"rectangular shaped\" graph", INT, true, true) << std::endl;

	std::cout << std::endl << "  POPULATION OPTIONS" << std::endl;
	std::cout << print_option ("\tPopulation size (default is 3)", INT, true, true) << std::endl;
	std::cout << print_option ("\tMigration rate", NUM, true, true) << std::endl;
	std::cout << print_option ("\t\tSets one migrant per generation", EMPTY, false, true) << std::endl;
	std::cout << print_option ("Reproductive limiting rate (default is 20)", INT, true, true) << std::endl;
	std::cout << print_option ("\tInbreeding depression rate (default is 0.0)", NUM, true, true) << std::endl;
	std::cout << print_option ("\t\tPurging coefficient (default is 0.0)", NUM, true, true) << std::endl;
	std::cout << print_option ("\tSet minimum age to sterility (0 by default, no aging)", INT, true, true) << std::endl;
	std::cout << print_option ("\t\tEnables male mating cost", EMPTY, false, true) << std::endl;
	std::cout << print_option ("\tMaximum number of movements allowed per mating season (default\n\t\t\t\tis the graph order)", INT, false, true) << std::endl;
	std::cout << print_option ("\tSet a maximum number of successful matings per season", INT, false, true) << std::endl;
	std::cout << print_option ("\tSet a maximum number encounters per season", INT, false, true) << std::endl;

	std::cout << std::endl << "  BIOLOGICAL TRAITS" << std::endl;
	std::cout << print_option ("\t\tGeneric value of additive variance (defaults are trait-specific)", NUM, false, true) << std::endl;
	std::cout << print_option ("\t\tGeneric value of environmental variance (defaults are trait-specific)", NUM, false, true) << std::endl;
	std::cout << std::endl << "  Body size" << std::endl;
	std::cout << print_option ("\t\tInitial body size (default is 5.0)", NUM, false, true) << std::endl;
	std::cout << print_option ("\tInitial body size for females (default is 5.0)", NUM, false, true) << std::endl;
	std::cout << print_option ("\tInitial body size for males (default is 5.0)", NUM, false, true) << std::endl;
	std::cout << print_option ("\tAdditive variance for body size (default is 1.0)", NUM, false, true) << std::endl;
	std::cout << print_option ("\tEnvironmental variance for body size (default is 1.0)", NUM, false, true) << std::endl;
	std::cout << std::endl << "  Fecundity" << std::endl;
	std::cout << print_option ("\tMinimum fecundity (referred to the additive value, default is 0.0)", NUM, false, true) << std::endl;
	std::cout << print_option ("\t\tCorrelation fecundity/trait (referred to the additive value, default is 1.0)", NUM, false, true) << std::endl;
	std::cout << print_option ("\tEnvironmental variance on fecundity", NUM, false, true) << std::endl;
	std::cout << std::endl << "  Sexual dimorphism for body size" << std::endl;
	std::cout << print_option ("Body size sexual dimorphism (default is 0.0)", NUM, true, true) << std::endl;
	std::cout << print_option ("\t\tAdditive variance for body size sexual dimorphism (default is 0.0)", NUM, false, true) << std::endl;
	std::cout << print_option ("\t\tEnvironmental variance for body size sexual dimorphism (default is 0.0)", NUM, false, true) << std::endl;
	std::cout << std::endl << "  Distance of movement" << std::endl;
	std::cout << print_option ("\tThe type of movement. Valid types are pseudo-brownian (`pb`) and\n\t\t\t\tone-step movement (`one`, `1`). Default is pseudo-brownian", TYPE, true, true) << std::endl;
	std::cout << print_complex ("dist.mov","\tMean and standard deviation of movement distance (default are 3 and 1)","NUM NUM") << std::endl;
	std::cout << print_complex ("dist.mov.fem","Mean and standard deviation of movement distance for females (default are 3 and 1)","NUM NUM") << std::endl;
	std::cout << print_complex ("dist.mov.mal","Mean and standard deviation of movement distance for males (default are 3 and 1)","NUM NUM") << std::endl;
	std::cout << print_option ("\tMaximum number of rounds that the path is saved on an edge (by \n\t\t\t\tdefault there's no limit)", INT, false, true) << std::endl;
	std::cout << print_option ("\t\tFemales won't leave any traceable path", EMPTY, false, true) << std::endl;

	std::cout << std::endl << "  MATE CHOICE PARAMETERS" << std::endl;
	std::cout << print_option ("\tInitial choice parameter (default is 0.0)", NUM, true, true) << std::endl;
	std::cout << print_option ("\t\tInitial choice parameter for females (default is 0.0)", NUM, false, true) << std::endl;
	std::cout << print_option ("\t\tInitial choice parameter for males (default is 0.0)", NUM, false, true) << std::endl;
	std::cout << print_option ("\t\tAdditive variance for the choice parameter (default is 0.1)", NUM, false, true) << std::endl;
	std::cout << print_option ("\t\tEnvironmental variance for the choice parameter (default is 0.1)", NUM, false, true) << std::endl;
	std::cout << print_option ("\tBias parameter (default is 0.0)", NUM, true, true) << std::endl;
	//std::cout << print_option ("\t\tInitial bias parameter for females (default is 0.0)", NUM, false, true) << std::endl;
	//std::cout << print_option ("\t\tInitial bias parameter for males (default is 0.0)", NUM, false, true) << std::endl;
	std::cout << print_option ("\t\tAdditive variance for the bias parameter (default is 0.0)", NUM, false, true) << std::endl;
	std::cout << print_option ("\t\tEnvironmental variance for the bias parameter (default is 0.0)", NUM, false, true) << std::endl;
	std::cout << print_option ("\tSet choosy males (the mating choice is made by males)", EMPTY, false, true) << std::endl;
	std::cout << print_option ("\tSet choosy females (the mating choice is made by females)", EMPTY, false, true) << std::endl;
	std::cout << print_option ("\tTolerance parameter (default is 0.1)", NUM, true, true) << std::endl;
	std::cout << print_option ("\tThe male-male competence model. Valid types are `size` and\n\t\t\t\t`random`. Default is size", TYPE, false, true) << std::endl;
	std::cout << print_option ("The preference model. The current implementation includes \n\t\t\t\tgaussian FND (`fnd`) and BD03 (`bd03`) models. It also includes\n\t\t\t\t`random` and `first-sight` models. Default is gaussian FND", TYPE, true, true) << std::endl;
	std::cout << print_option ("\t\tEnable monogamy", EMPTY, false, true) << std::endl;

	std::cout << std::endl << "  RANDOM ENCOUNTERS" << std::endl;
	std::cout << print_option ("\t\tSet random encounters (i.e. remove spatial factor)", EMPTY, true, true) << std::endl;
	std::cout << print_option ("\t\tUnder random mating, all individuals from one gender try to mate,\n\t\t\t\tand can encounter every individual of the other gender\n\t\t\t\t(until they mate one, or none)", EMPTY, false, true) << std::endl;

	std::cout << std::endl << "  OUTPUT OPTIONS" << std::endl;
	std::cout << print_option ("\tSave all individual meetings in registry fail, including unsiccesful ones", EMPTY, false, true) << std::endl;

	std::cout << std::endl;
	std::cout << "EXAMPLES: The following are examples of use." << std::endl;
	std::cout << std::endl;
	std::cout << print_example ({"-N100","-V1000","-s1234"}) << std::endl;
	std::cout << std::endl;
	std::cout << print_example ({"-N100","-V1000","--seed=1234", "--season-time=100"}) << std::endl;
	std::cout << std::endl;
	std::cout << print_example ({"-N100","-V1000","-s1234", "--random", "--n-matings=100"}) << std::endl;
	std::cout << std::endl;
	std::cout << print_example ({"-N 100","-V 1000","-s 1234", "--M one"}) << std::endl;
	std::cout << std::endl;
	std::cout << print_example ({"-N 100","-V 1000","-s 1234", "--dist.size.fem 5 0", "--dist.size.mal=4 1"}) << std::endl;

	std::cout << std::endl;
	std::cout << "More help in the README file or in https://gitlab.com/elcortegano/mate-trace" << std::endl;
	exit(0);
}

/* ***************************************************************************************
 * Log
 * ***************************************************************************************/

/* save_log: Saves a log of the options used, indicated if match the defaults or not
 * --------------------------------------------------------------------------------------*/
void Settings::save_log () {

	std::string filename ("logfile.txt");
	std::ofstream file;
	file.open(filename);

	file << "MATE-TRACE LOG FILE" << std::endl;
	file << "Only non-default values are recorded" << std::endl;

	file << std::endl << "GENERAL OPTIONS" << std::endl;
	file << "  Seed: " << parameters.seed << std::endl;
	if (parameters.t != DEFAULT_T) file << "  Generations: " << parameters.t << std::endl;
	if (parameters.iterate != DEFAULT_ITER) file << "  Iterate" << std::endl;

	file << std::endl << "MAP OPTIONS" << std::endl;
	if (!mc_info.random_encounters) {
		if (map.shape != DEFAULT_SHAPE) file << "  Map shape: " << print_shape(map.shape) << std::endl;
		if (map.N_vertices != DEFAULT_MAPSIZE) file << "  Map size: " << map.N_vertices << std::endl;
		if (map.length != DEFAULT_MAP_LENGTH) file << "  Length: " << map.length << std::endl;
		if (map.width != DEFAULT_MAP_WIDTH) file << "  Width: " << map.width << std::endl;
	} else file << "  No map: random matings used" << std::endl;

	file << std::endl << "POPULATION OPTIONS" << std::endl;
	if (pop_info.N_inds != DEFAULT_N) file << "  Population size: " << pop_info.N_inds << std::endl;
	if (pop_info.movement != DEFAULT_MOVE_TYPE) file << "  Movement type: " << print_movement(pop_info.movement) << std::endl;
	if (pop_info.migration != DEFAULT_MIGRATION) file << "  Migration rate: " << pop_info.migration << std::endl;
	if (pop_info.R != DEFAULT_R) file << "  Reproductive limiting rate: " << pop_info.R << std::endl;
	if (genetics.delta != DEFAULT_DELTA) file << "  Inbreeding depression rate: " << genetics.delta << std::endl;
	if (genetics.d != DEFAULT_D) file << "  Purging coefficient: " << genetics.d << std::endl;
	if (pop_info.aging != DEFAULT_AGING) file << "  Aging: " << pop_info.aging << std::endl;
	if (pop_info.season_time != DEFAULT_SEASON_TIME) file << "  Season time: " << pop_info.season_time << std::endl;
	if (pop_info.max_nmatings != DEFAULT_NMATINGS) file << "  Number of matings: " << pop_info.max_nmatings << std::endl;
	if (pop_info.max_nmeetings != DEFAULT_NMEETINGS) file << "  Number of encounters: " << pop_info.max_nmeetings << std::endl;

	file << std::endl << "BIOLOGICAL TRAITS" << std::endl;
	file << "  Body size: " << std::endl;
	if (genetics.size.Va != DEFAULT_VA || genetics.size.Ve != DEFAULT_VE || genetics.size.mean_fem != DEFAULT_MEAN_SIZE || genetics.size.mean_mal != DEFAULT_MEAN_SIZE) {
		file << "  - Female mean: " << genetics.size.mean_fem << std::endl;
		file << "  - Male mean: " << genetics.size.mean_mal << std::endl;
		file << "  - Additive variance: " << genetics.size.Va << std::endl;
		file << "  - Environmental variance: " << genetics.size.Ve << std::endl;
	}
	file << "  Distribution of the distance moved: " << std::endl;
	if (genetics.dist.mean_fem != DEFAULT_MEAN_DIS || genetics.dist.mean_mal != DEFAULT_MEAN_DIS || genetics.dist.sd_fem != DEFAULT_SD_DIS || genetics.dist.sd_mal != DEFAULT_SD_DIS) {
		file << "  - Female mean distance: " << genetics.dist.mean_fem << std::endl;
		file << "  - Female sd distance: " << genetics.dist.sd_fem << std::endl;
		file << "  - Male mean distance: " << genetics.dist.mean_mal << std::endl;
		file << "  - Male sd distance: " << genetics.dist.sd_mal << std::endl;
	}
	if (map.path_time != DEFAULT_DURABILITY) file << "  Path time: " << map.path_time << std::endl;
	if (map.track_path != DEFAULT_TRACKPATH) file << "  No path" << std::endl;

	file << std::endl << "MATE CHOICE PARAMETERS" << std::endl;
	file << "  Choice parameter: " << std::endl;
	if (genetics.choice.Va != DEFAULT_S || genetics.choice.Ve != DEFAULT_S || genetics.choice.mean_fem != DEFAULT_MEAN_C || genetics.choice.mean_mal != DEFAULT_MEAN_C) {
		file << "  - Female mean: " << genetics.choice.mean_fem << std::endl;
		file << "  - Male mean: " << genetics.choice.mean_mal << std::endl;
		file << "  - Additive variance: " << genetics.choice.Va << std::endl;
		file << "  - Environmental variance: " << genetics.choice.Ve << std::endl;
	}
	file << "  Bias parameter: " << std::endl;
	if (genetics.bias.Va != DEFAULT_SD_B || genetics.bias.Ve != DEFAULT_SD_B || genetics.bias.mean_fem != DEFAULT_MEAN_B || genetics.bias.mean_mal != DEFAULT_MEAN_B) {
		file << "  - Female mean: " << genetics.bias.mean_fem << std::endl;
		file << "  - Male mean: " << genetics.bias.mean_mal << std::endl;
		file << "  - Additive variance: " << genetics.bias.Va << std::endl;
		file << "  - Environmental variance: " << genetics.bias.Ve << std::endl;
	}
	if (mc_info.S != DEFAULT_S) file << "  Tolerance: " << mc_info.S << std::endl;
	if (mc_info.mm_competence != DEFAULT_MMCOMPETENCE) file << "  Male-male competence: " << print_mmc(mc_info.mm_competence) << std::endl;
	if (mc_info.preference != DEFAULT_PREFERENCE) file << "  Preference model: " << print_pmodel(mc_info.preference) << std::endl;
	if (pop_info.monogamy != DEFAULT_MONOGAMY) file << "  Monogamy" << std::endl;

	file << std::endl << "RANDOM MATING" << std::endl;
	if (mc_info.random_encounters != DEFAULT_ENCOUNTERS) file << "  Random encounters enabled" << std::endl;
	if (mc_info.thm) file << "  Try-hard matings enabled" << std::endl;
	file << std::endl;
}


/* print_option: Prints documentation for a command line terminal option.
 * --------------------------------------------------------------------------------------*/
std::string Settings::print_option (std::string message, std::string argument, bool short_opt, bool long_opt) {

	static ent index (0);

	std::string line ("  ");
	if (short_opt) {
		line += ("-");
		line += long_options[index].val;
	}
	if (short_opt & long_opt) line += ", ";
	if (long_opt) {
		line += "--";
		line += long_options[index++].name;
	}
	if (argument != "") line += SPACE + '<' + argument + '>';
	line += TAB;
	line += message;
	line += ".";

	return line;
}

/* print_complex: Prints documentation for complex command line terminal options.
 * --------------------------------------------------------------------------------------*/
std::string Settings::print_complex (std::string optname, std::string message, std::string argument) {

	std::string line ("  --");
	line += optname;
	if (argument != "") line += SPACE + '<' + argument + '>';
	line += TAB;
	line += message;
	line += ".";

	return line;
}

/* print_example: Prints examples of use.
 * --------------------------------------------------------------------------------------*/
std::string Settings::print_example (std::vector<std::string> options) {

	std::string line ("  ");
	line += "mate-trace";
	for (auto& opt: options) line += SPACE + opt;

	return line;
}

/* ***************************************************************************************
 * Read specific options
 * ***************************************************************************************/

/* parse_complex: Identifies if options with multiple arguments are passed, and parses them
 * conveniently.
 * --------------------------------------------------------------------------------------*/
void Settings::parse_complex (std::vector<std::string>& args) {
	for (ent i(1); i<args.size(); ++i) {
		std::string arg_name (std::string(args[i]));
		std::size_t n_equal = std::count(arg_name.begin(), arg_name.end(), '=');
		std::vector<std::string> v_arg (split_string(arg_name, '='));
		if (n_equal>1) throw_error (ERROR_SETNG_OPT_EQU);
		else if (n_equal) {
			arg_name = v_arg[0];
			args[i] = arg_name;
			args.insert(args.begin()+i+1, v_arg[1]);
		}
		if (arg_name==OPT_DIST_MOV || arg_name==OPT_DIST_MOV_FEM || arg_name==OPT_DIST_MOV_MAL) {
			parse_dist_mov(args, i);
		}
	}
}

/* parse_dist_mov: Reads options to set movement distributions
 * --------------------------------------------------------------------------------------*/
void Settings::parse_dist_mov (std::vector<std::string>& args, ent& i) {
	if ((args.size()-i) <= 2) throw_error (ERROR_SETNG_DISTRIB);
	std::string arg_name = std::string (args[i]);
	if (arg_name==OPT_DIST_MOV) {
		set_dist_mov = true;
		if (set_dist_mov & (set_dist_mov_fem | set_dist_mov_mal)) throw_error (ERROR_SETNG_DIMORPH);
		if (isNumeric(args[i+1])) {
			genetics.dist.mean_fem = atof(args[i+1].c_str());
			genetics.dist.mean_mal = genetics.dist.mean_fem;
			args.erase(args.begin()+i+1);
		} else throw_error (ERROR_SETNG_DISTRIB);
		if (isNumeric(args[i+1])) {
			genetics.dist.sd_fem = atof(args[i+1].c_str());
			genetics.dist.sd_mal = genetics.dist.sd_fem;
			args.erase(args.begin()+i+1);
		} else throw_error (ERROR_SETNG_DISTRIB);
	} else if (arg_name==OPT_DIST_MOV_FEM) {
		set_dist_mov_fem = true;
		if (set_dist_mov & (set_dist_mov_fem | set_dist_mov_mal)) throw_error (ERROR_SETNG_DIMORPH);
		if (isNumeric(args[i+1])) {
			genetics.dist.mean_fem = atof(args[i+1].c_str());
			args.erase(args.begin()+i+1);
		} else throw_error (ERROR_SETNG_DISTRIB);
		if (isNumeric(args[i+1])) {
			genetics.dist.sd_fem = atof(args[i+1].c_str());
			args.erase(args.begin()+i+1);
		} else throw_error (ERROR_SETNG_DISTRIB);
	} else if (arg_name==OPT_DIST_MOV_MAL) {
		set_dist_mov_mal = true;
		if (set_dist_mov & (set_dist_mov_fem | set_dist_mov_mal)) throw_error (ERROR_SETNG_DIMORPH);
		if (isNumeric(args[i+1])) {
			genetics.dist.mean_mal = atof(args[i+1].c_str());
			args.erase(args.begin()+i+1);
		} else throw_error (ERROR_SETNG_DISTRIB);
		if (isNumeric(args[i+1])) {
			genetics.dist.sd_mal = atof(args[i+1].c_str());
			args.erase(args.begin()+i+1);
		} else throw_error (ERROR_SETNG_DISTRIB);
	}
	args.erase(args.begin()+i);
	--i;
}

/* read_graph_shape: Read the topology of the graph to be built.
 * --------------------------------------------------------------------------------------*/
void Settings::read_graph_shape ( std::string arg) {
	std::transform(arg.begin(), arg.end(), arg.begin(), ::tolower);
	if (arg=="r" || arg=="rectangular" || arg=="s" || arg=="square") map.shape = RECTANGULAR;
	else if (arg=="ba" || arg=="barabasi" || arg=="barabasi-albert") map.shape = BA;
	else throw_error(ERROR_SETNG_TOPOOPT);
}

/* read_move_type: Read the type of movement option.
 * --------------------------------------------------------------------------------------*/
void Settings::read_move_type ( std::string arg) {
	std::transform(arg.begin(), arg.end(), arg.begin(), ::tolower);
	if (arg=="1" || arg=="one") pop_info.movement = ONE;
	else if (arg=="pseudo-brownian" || arg=="pseudo-b" || arg=="pb" || arg=="p") pop_info.movement = PSEUDO_B;
	else throw_error(ERROR_SETNG_MOVEOPT);
}

/* read_mm_comp: Read the male-male competence model.
 * --------------------------------------------------------------------------------------*/
void Settings::read_mm_comp (std::string arg) {
	std::transform(arg.begin(), arg.end(), arg.begin(), ::tolower);
	if (arg=="size") mc_info.mm_competence = SIZE;
	else if (arg=="random") mc_info.mm_competence = RANDOM;
	else throw_error(ERROR_SETNG_MMCMOPT);
}

/* read_preference: Read the model of preference.
 * --------------------------------------------------------------------------------------*/
void Settings::read_preference ( std::string arg) {
	std::transform(arg.begin(), arg.end(), arg.begin(), ::tolower);
	if (arg=="gaussian-fnd" || arg=="fnd") mc_info.preference = GAUSSIAN_FND;
	else if (arg=="gaussian-bd03" || arg=="bd03") mc_info.preference = GAUSSIAN_BD03;
	else if (arg=="random") mc_info.preference = BINARY_05;
	else if (arg=="first-sight") mc_info.preference = FIRST_SIGHT;
	else throw_error(ERROR_SETNG_PREFOPT);
}
