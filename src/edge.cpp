/* =======================================================================================
 *                                                                                       *
 *       Filename: edge.cpp                                                              *
 *                                                                                       *
 *    Description: Defines the properties of edges of the graph. That is, the paths      *
 *                 followed by the females.                                              *
 *                                                                                       *
 ======================================================================================= */

#include "edge.h"

/* ********************************************************************************************
 * Constructor & destructor
 * ********************************************************************************************/
Edge::Edge ( ent unique_ID, ent node1, ent node2) {

	ID = unique_ID;
	vertex1 = node1;
	vertex2 = node2;
	width = 0;
	time = 0;

}

/* set_durability: Initialize static attribute durability
 * -------------------------------------------------------------------------------------------*/
ent Edge::durability = DEFAULT_DURABILITY;

void Edge::set_durability (ent dur) {
	durability = dur;
}

/* set_traceability: Initialize static attribute traceable
 * -------------------------------------------------------------------------------------------*/
bool Edge::traceable = DEFAULT_TRACKPATH;

void Edge::set_traceability (bool trace) {
	traceable = trace;
}

/* ********************************************************************************************
 * Set methods
 * ********************************************************************************************/

/* track_path: Saves information on the edge, including direction and width of the path.
 * -------------------------------------------------------------------------------------------*/
void Edge::track_path ( Individual* ind, ent from, ent to) {
	if (!traceable) return;
	width = ind->ask_size();
	vertex1 = from;
	vertex2 = to;
	time = 0;
}

/* clear_old_paths: Edges keep paths in memory a limited time.
 * -------------------------------------------------------------------------------------------*/
bool Edge::clear_old_path () {
	if (!traceable) return true;
	else if (durability) {
		++time;
		if (time>=durability) {
			width = 0;
			time = 0;
			return true;
		}
	}
	return false;
}
