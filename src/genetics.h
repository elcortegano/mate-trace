#ifndef GENETICS_H
#define GENETICS_H

#include "alias.h"
#include "settings.h"


// =====   FORWARD DECLARATIONS   =====
class Individual;
struct Pedigree_record;


// =====   GLOBAL VARIABLES   =====
constexpr coefficient DEFAULT_EE = 0.0;

// =====   EXTERN VARIABLES   =====
extern std::mt19937 rnd_generator;


// ====   CLASSES   ====
class Trait { // It represents an additive trait

friend class Fitness;

protected:

	Gaussian dist; // trait distribution
	coefficient A; // additive value
	coefficient G; // genotypic value (affected by inbreeding depression)
	coefficient P; // phenotypic value
	coefficient E_E; // mean environmental effect

public:

	Trait () {};
	Trait ( const Trait_parameters& , bool );
	Trait ( const Trait& , const Trait& , coefficient , coefficient , const Trait_parameters& );

	coefficient value_A () const { return A; }
	coefficient value_P () const { return P; }
	void mask_P (coefficient x) { P += x; }

protected:

	void inherite_additive (const Trait& , const Trait& , coefficient , coefficient , coefficient);
	coefficient compute_env (coefficient sd) { Gaussian dist_env (E_E, sd); return dist_env (rnd_generator); }
	void compute_genotype () { G = A; }
	void compute_phenotype (coefficient sd_E) { P = G + compute_env(sd_E); }
};

class Fitness: public Trait {

protected:

public:

	Fitness () {};
	Fitness ( const Trait& , const Genetic_parameters&, coefficient );
	Fitness ( const Trait& , const Trait& , coefficient , coefficient , const Genetic_parameters& , coefficient );

protected:

	void compute_genotype ( coefficient delta , coefficient g ) { G = A * exp(-delta*g); }
};

class Survival {

friend class Individual;

	protected:
	boost::math::normal dist;
	coefficient maxp;

	public:
	Survival () { dist = boost::math::normal(0.0, 1.0); maxp = 0.0; };
	void set (coefficient m, coefficient s) {
		dist = boost::math::normal(m, s);
		maxp = pdf(dist, m);
	}
	bool sim_survival (const Individual& i);

	protected:
	coefficient psurvival (coefficient x) { return pdf(dist,x)/maxp; }
};

// =====   FUNCTION'S PROTOTYPES   =====
Deck2D initialize_coancestry ( ent );
coefficient g_AB ( Individual* , const Pedigree_record& , const Deck2D& , coefficient , ent );
coefficient f_AB ( Individual* , const Pedigree_record& , const Deck2D& , ent );

// =====   INLINE FUNCTIONS   =====
inline coefficient V_M (coefficient Ve) { return Ve * 0.001; }

#endif
