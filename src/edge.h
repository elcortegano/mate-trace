#ifndef EDGE_H
#define EDGE_H


#include "alias.h"
#include "individual.h"
#include "settings.h"


// ====   CLASSES   ====
class Edge {

protected:

	ent ID; // unique ID of the vertex
	ent vertex1; // the ID of the vertex of origin
	ent vertex2; // the ID of the destination vertex
	coefficient width; // a value of width or weight of the edge
	static ent durability; // maximum number of rounds the edge can save the width value
	static bool traceable; // a traceable edge can save information beyond neighbour vertices
	ent time; // the time that a path has been saved

public:

	// Constructor & destructor
	Edge ( ent , ent , ent );

	// Individual-interactivity methods
	void track_path ( Individual* , ent , ent );

	// Get methods
	ent get_ID () const { return ID; }
	ent get_vertex1 () const { return vertex1; }
	ent get_vertex2 () const { return vertex2; }
	coefficient get_width () const { return width; }

	// Set methods
	void reset_width() { width = 0; }
	bool clear_old_path ();
	void set_durability ( ent );
	void set_traceability ( bool );

protected:

};

#endif
