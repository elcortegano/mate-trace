#ifndef MCHOICE_H
#define MCHOICE_H

#include "alias.h"
#include "settings.h"
#include <list>


// =====   GLOBAL, EXTERN & STATIC VARIABLES   =====
extern std::mt19937 rnd_generator;

class Population;
class Individual;


// ====   CLASSES   ====
class MChoice {

protected:

	Matechoice_parameters mc_info; // mate choice basic settings
	coefficient tolerance;
	bool c_impatience; // compute impatince for mating?
	bool calc_f; // preference values must be calculated from phenotypes
	ent n_mates; // counting of the number of mates
	coefficient D_max; // maximum phenotypic difference
	Matrix f_matrix; // matrix of preference values
	coefficient f_max; // maximum preference

public:

	// Constructor & destructor
	MChoice () {};
	MChoice ( Population& , const Matechoice_parameters& );
	void init ( Population& , const Matechoice_parameters& );
	void update ( Population& );

	// Competence models
	std::list<Individual*>::iterator mm_competence ( std::list<Individual*> );
	Vector comp_size ( std::list<Individual*> );
	Vector comp_random ( std::list<Individual*> );

	// Preference models
	bool match ( Individual* , Individual* );

	// Get methods
	Matechoice_parameters get_mc () const { return mc_info; }

protected:

	// Mate choice parameters
	void calculate_nmates ( Population& );
	void calculate_Dmax ( Population& );

	// Preference models
	coefficient generic ( coefficient , coefficient , coefficient );
	coefficient calc_gaussian_preference ( Individual* , Individual* );
	coefficient alpha_FND ( coefficient , coefficient );
	coefficient alpha_BD03 ( coefficient , coefficient );
	void calculate_f_matrix ( Population& );
	void calculate_fmax ( Population& );

};

#endif
